/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <memory>
#include <string_view>
#include <string>

namespace fgs {

class SW_application{
public:
    using Size = unsigned;

    SW_application();
    virtual ~SW_application();

    const std::string& title() const;
    SW_application& title(std::string_view p_title);

    SW_application& size(Size width_p, Size height_p);
    Size width() const;
    Size height() const;

    /// Sets whether the cursor lock is enabled
    ///
    /// the cursor lock sets GLFW_CURSOR to GLFW_CURSOR_DISABLED
    /// if there is no existing window (i.e. run() wasn't called) the call is ignored
    SW_application& cursor_lock(bool enabled);
    bool            cursor_lock() const;

    void run();

    /// Checks if the application owns an existing window
    bool has_window() const;

    struct Cursor_position{ double x,y; };

    /// Returns the position of the cursor relative to the clien area of the owned window.
    ///
    /// if the application does't own a window returns {0,0}
    Cursor_position cursor_position() const;

    void vsync( bool enabled );
    bool vsync() const;

    enum class Action{
        PRESS,
        RELEASE
    };

    /// Returns the state of the given key (i.e. the last reported state)
    Action key_state(int key);

    bool full_screen() const;
    void full_screen(bool value);

protected:
    SW_application(SW_application&& other);

private:
    void draw();
    void init();
    void validate_vsync();
    void validate_full_screen();

    struct FS_info{
        int width, height, refresh_rate;
    };

    FS_info get_fs_info() const;

    virtual void do_init(Size width, Size height) = 0;
    virtual void do_draw() = 0;
    virtual void on_resize([[maybe_unused]] Size width, [[maybe_unused]] Size height){}

    virtual void on_key([[maybe_unused]] int key, [[maybe_unused]]  int mods, [[maybe_unused]] Action action) {}

    virtual void on_mouse_btn(int btn, int mods, double x, double y,Action action) {
        (void)btn;(void)mods;(void)x;(void)y,(void)action;
    }

    virtual void on_mouse_move(double x, double y) {
        (void)x;(void)y;
    }

    // returns false if glfw_action neither GLFW_RELEASE nor GLFW_PRESS
    static bool convert_action(int glfw_action, Action* out);

    struct Data;
    Data&       data()          { return  *m_;}
    const Data& data() const    { return  *m_;}
    std::unique_ptr<Data> m_;
};

} // namespace fgs
