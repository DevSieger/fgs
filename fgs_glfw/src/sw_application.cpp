/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/sw_application.hpp>
#include <fgs/common.hpp>
#include <GLFW/glfw3.h>
#include <stdexcept>
#include <cassert>
#include <string>
#include <dk/logging.hpp>

namespace fgs {

namespace  {

// singletone to make sure that glfw is initialized
class GLFW_system{
public:
    static void init_once(){
        // logging should be initialized first because a life time of the logging
        // system have to cover a life time of GLFW_system.
        (void)dk::get_logger(); // initializes logging singletone
        static GLFW_system singletone;
    }

private:
    GLFW_system(){
        if(!glfwInit() ){
            throw std::runtime_error("can't initialize GLFW");
        }
        auto error_cb = []( int err, const char* dsc) -> void {
            dk::log(dk::Detail_level::CRITICAL,"GLFW error(%1%): %2%",err,dsc);
        };
        glfwSetErrorCallback(error_cb);
    }
    ~GLFW_system(){
        glfwTerminate();
    }
};

SW_application& get_sw_app(GLFWwindow* wnd){
    void* ptr   = glfwGetWindowUserPointer(wnd);
    assert(ptr);
    return *reinterpret_cast<SW_application*>(ptr);
};

}

struct SW_application::Data{
    std::string title;
    GLFWwindow* wnd     = nullptr;
    unsigned width      = 1024;
    unsigned height     = 720;
    // for the full screen mode
    int last_x, last_y, last_w, last_h;
    bool vsync          = true;
    bool full_screen    = false;
};

SW_application::SW_application(){
    m_ = std::make_unique<Data>();
}

SW_application::SW_application(SW_application&& other):
    SW_application()
{
    using std::swap;
    swap(m_, other.m_);
}

SW_application::~SW_application()
{
    if( m_  && data().wnd ){
        glfwDestroyWindow(data().wnd);
    }
}

const std::string& SW_application::title() const{
    return data().title;
}

SW_application::Size SW_application::width() const
{
    return data().width;
}

SW_application::Size SW_application::height() const
{
    return data().height;
}

SW_application& SW_application::size(SW_application::Size width_p, SW_application::Size height_p)
{
    auto& d     = data();

    if(d.wnd){
        glfwSetWindowSize(d.wnd, width_p, height_p);
        glfwGetWindowSize(d.wnd, &d.last_w, &d.last_h);
        d.width     = d.last_w;
        d.height    = d.last_h;
    } else {
        d.last_w    = d.width   = width_p;
        d.last_h    = d.height  = height_p;
    }

    return *this;
}

SW_application& SW_application::title(std::string_view p_title){
    data().title = p_title;
    return *this;
}

void SW_application::run()
{
    assert(m_);
    init();
    while(!glfwWindowShouldClose(data().wnd)){
        glfwPollEvents();
        draw();
    }
}

void SW_application::draw()
{
    glfwMakeContextCurrent(data().wnd);
    do_draw();
    glfwSwapBuffers(data().wnd);
}

bool SW_application::convert_action(int glfw_action, Action* out)
{
    assert(out);
    if(glfw_action == GLFW_PRESS){
        *out = Action::PRESS;
    }else if (glfw_action==GLFW_RELEASE) {
        *out = Action::RELEASE;
    }else{
        return false;
    }
    return true;
}

void SW_application::init()
{
    GLFW_system::init_once();
    auto& d = data();

    int gl_version[2] = {4,3};
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,gl_version[0]);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,gl_version[1]);
    glfwWindowHint(GLFW_RESIZABLE,1);
    glfwWindowHint(GLFW_SRGB_CAPABLE,1);
    glfwWindowHint(GLFW_DOUBLEBUFFER,1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    if(d.full_screen){
        GLFWmonitor* monitor    = glfwGetPrimaryMonitor();
        const auto fs_info      = get_fs_info();
        d.wnd = glfwCreateWindow( fs_info.width, fs_info.height, d.title.c_str(), monitor, nullptr);

        // position {0,0} in the windowed mode is annoying.
        // this one fixes it,
        d.last_x = d.last_y = 32;
    }else{
        d.wnd = glfwCreateWindow( d.width, d.height, d.title.c_str(), nullptr, nullptr);
    }

    if( !d.wnd ){
        throw std::runtime_error("can't create window");
    }

    glfwSetWindowUserPointer(d.wnd,this);

    auto key_evnt_cb = [](GLFWwindow* wnd, int key, int, int act, int mods){
        Action action;
        if(convert_action(act,&action)){
            get_sw_app(wnd).on_key(key,mods,action);
        }
    };
    auto mbtn_evnt_cb = [](GLFWwindow* wnd, int btn,int act, int mods){
        Action action;
        // always true
        if(convert_action(act,&action)){
            SW_application& app = get_sw_app(wnd);
            double x,y;
            glfwGetCursorPos(app.data().wnd,&x,&y);
            app.on_mouse_btn(btn,mods,x,y,action);
        }
    };
    auto cm_evnt_cb = [](GLFWwindow* wnd, double x, double y){
        get_sw_app(wnd).on_mouse_move(x,y);
    };

    auto fb_rsz_cb  = [](GLFWwindow* wnd, int w, int h) -> void{
        get_sw_app(wnd).on_resize(w,h);
    };

    auto wnd_rsz_cb = [](GLFWwindow* wnd, int w, int h) -> void{
        auto& d = get_sw_app(wnd).data();
        d.width     = w;
        d.height    = h;
    };

    glfwSetKeyCallback(d.wnd,key_evnt_cb);
    glfwSetMouseButtonCallback(d.wnd,mbtn_evnt_cb);
    glfwSetCursorPosCallback(d.wnd,cm_evnt_cb);
    glfwSetFramebufferSizeCallback(d.wnd,fb_rsz_cb);
    glfwSetWindowSizeCallback(d.wnd,wnd_rsz_cb);

    glfwMakeContextCurrent(d.wnd);

    // multiple initialization should'nt harm if pointers to real gl functions is the same for every context
    fgs::initialize_ogl_fn();
    int w,h;

    glfwGetWindowSize(d.wnd, &w, &h);
    d.width     = w;
    d.height    = h;

    glfwGetFramebufferSize(d.wnd,&w,&h);

    validate_vsync();
    do_init(w,h);
}

SW_application& SW_application::cursor_lock(bool enabled)
{
    if(data().wnd){
        int mode = enabled? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL;
        glfwSetInputMode(data().wnd,GLFW_CURSOR,mode);
    }
    return *this;
}

bool SW_application::cursor_lock() const
{
    int mode = data().wnd ? glfwGetInputMode(data().wnd,GLFW_CURSOR) : GLFW_CURSOR_NORMAL;
    return mode == GLFW_CURSOR_DISABLED;
}

SW_application::Cursor_position SW_application::cursor_position() const
{
    Cursor_position res{0,0};
    if(has_window()){
        glfwGetCursorPos(data().wnd,&res.x,&res.y);
    }
    return res;
}

SW_application::Action SW_application::key_state(int key)
{
    Action res { Action::RELEASE };
    if(has_window()){
        int status  = glfwGetKey(data().wnd,key);
        bool succ   = convert_action(status,&res);
        assert(succ);
    }
    return res;
}

bool SW_application::full_screen() const
{
    return data().full_screen;
}

void SW_application::full_screen(bool value)
{
    auto& d = data();
    if(value != d.full_screen){
        d.full_screen = value;
        validate_full_screen();
    }
}

void SW_application::validate_full_screen()
{
    auto& d = data();
    if(d.wnd && d.full_screen != (glfwGetWindowMonitor(data().wnd) != NULL) ){
        if(!d.full_screen){
            glfwSetWindowMonitor(d.wnd, NULL, d.last_x, d.last_y, d.last_w, d.last_h,0);
        }else{
            glfwGetWindowPos(d.wnd, &d.last_x, &d.last_y);
            glfwGetWindowSize(d.wnd,&d.last_w, &d.last_h);
            GLFWmonitor* monitor    = glfwGetPrimaryMonitor();
            const auto fs_info      = get_fs_info();
            glfwSetWindowMonitor(d.wnd, monitor, 0, 0, fs_info.width, fs_info.height, fs_info.refresh_rate);
        }
        validate_vsync();
    }
}

bool SW_application::vsync() const
{
    return data().vsync;
}

void SW_application::vsync(bool enabled)
{
    data().vsync    = enabled;
    validate_vsync();
}

void SW_application::validate_vsync()
{
    if(data().wnd){
        glfwMakeContextCurrent(data().wnd);
        glfwSwapInterval( data().vsync ? 1 : 0 );
    }
}

SW_application::FS_info SW_application::get_fs_info() const
{
    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    return {mode->width, mode->height, mode->refreshRate};
}

bool SW_application::has_window() const
{
    return data().wnd;
}

} // namespace fgs
