/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once

#if defined(GRAPHIC_GLES)
#include <GLES2/gl2.h>

bool initialize_ogl_fn(){ return true; }
#else
#include <fgs/gl_fun/gl_core_uq_4_3.hpp>

namespace fgs {

/// initializes OpenGL "functions"
bool initialize_ogl_fn();

namespace dtl {
// do not call this directly, use macro
bool check_gl_error_f(char const * file,int line);
} // namespace dtl

#ifndef FGS_NO_GL_ERROR_CHECKING
#define check_gl_error(x) fgs::dtl::check_gl_error_f(__FILE__,__LINE__)
#else
// for release build
#define check_gl_error(x) false
#endif

} // namespace fgs

#endif
