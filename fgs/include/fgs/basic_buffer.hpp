/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/common.hpp>
#include <memory>

// a separate header for the "static" part of buffers.
// the template part is placed in buffer.hpp

namespace fgs {

//required version of opengl: 1.5
class Buffer_storage: public std::enable_shared_from_this<Buffer_storage>{
public:
    static std::shared_ptr<Buffer_storage> construct(GLenum a_usage = GL_STREAM_DRAW);

    Buffer_storage(Buffer_storage&& source) = delete;

    ~Buffer_storage();

    GLuint id() const{
        return id_;
    }

    void reallocate(size_t new_size);

    template<typename Value>
    void upload(size_t offset, const Value& value){
        bind();
        glBufferSubData(type(),offset,sizeof(Value),std::addressof(value));
        check_gl_error();
    }
    template<typename Value>
    void upload(size_t offset, const Value* value, size_t count){
        bind();
        glBufferSubData(type(),offset,sizeof(Value)*count,value);
        check_gl_error();
    }

    size_t size_align() const{
        return size_align_;
    }
    void size_align( size_t new_align ){
        if(new_align){
            size_align_=new_align;
        }else{
            size_align_=1;
        }
    }

    size_t allocated() const{
        return allocated_;
    }

    void usage(GLenum new_usage){
        usage_=new_usage;
    }
    GLenum usage() const{
        return usage_;
    }

private:
    Buffer_storage(GLenum a_usage);

    // for inner purposes
    void bind(){
        glBindBuffer(type(),id());
        check_gl_error();
    }

    static constexpr GLenum type(){
        return GL_COPY_READ_BUFFER;
    }


    size_t get_allocation_size(size_t requested_size) const{
        size_t s=requested_size+size_align()-1;
        return s-(s%size_align());
    }
    size_t allocated_   = 0;
    size_t size_align_  = 64;
    GLenum usage_       = GL_STREAM_DRAW;
    GLuint id_          = 0;
};

class Buffer{
public:
    Buffer(GLenum a_type, GLenum a_usage = GL_STREAM_DRAW);
    Buffer(GLenum a_type, Buffer_storage& storage);

    Buffer(Buffer&& source) = default;

    virtual ~Buffer() = default;

    GLuint id() const{
        return storage_->id();
    }
    GLenum type() const{
        return type_;
    }

    void bind(){
        glBindBuffer(type(),id());
        check_gl_error();
    }

    Buffer_storage& storage(){
        return *storage_;
    }

    const Buffer_storage& storage() const {
        return *storage_;
    }

private:
    const GLenum type_;
    std::shared_ptr<Buffer_storage> storage_;
};

//required version of opengl: 3.0
/// auxiliary class that binds a buffer to the indexed binding point
class Ibp_buffer: public Buffer{
public:
    Ibp_buffer(GLenum a_type, GLuint a_binding_point, GLenum a_usage = GL_STREAM_DRAW);

    Ibp_buffer(GLenum a_type, GLuint a_binding_point, Buffer_storage& storage);

    GLuint binding_point()const{
        return binding_point_;
    }

    void binding_point(GLuint a_binding_point){
        // reset binding for binding_point_
        glBindBufferBase(type(),binding_point_, 0);
        check_gl_error();

        binding_point_=a_binding_point;
        return bind_to_ibp();
    }

    GLintptr binding_offset()const{
        return binding_offset_;
    }
    GLintptr binding_size()const{
        return binding_size_;
    }

    /* let ogl do error checking :P */
    void bind_range(GLintptr offset,GLsizeiptr data_size){
        binding_offset_ = offset;
        binding_size_   = data_size;
        bind_to_ibp();
    }
    void bind_entire(){
        binding_offset_ = 0;
        binding_size_   = 0;
        bind_to_ibp();
    }

protected:
    void bind_to_ibp();

private:
    GLuint      binding_point_;
    GLintptr    binding_offset_ = 0;
    GLsizeiptr  binding_size_   = 0;
};

class Shader_program;

//required version of opengl: 3.1
class UBO_base: public Ibp_buffer{
public:
    UBO_base(GLuint a_binding_point, GLenum a_usage = GL_STREAM_DRAW):
        Ibp_buffer(GL_UNIFORM_BUFFER,a_binding_point, a_usage)
    {
        init();
    }

    UBO_base(GLuint a_binding_point, Buffer_storage& storage):
        Ibp_buffer(GL_UNIFORM_BUFFER,a_binding_point, storage)
    {
        init();
    }

    void set_binding( Shader_program const & shader, GLuint uniform_block_index);
    void set_binding( Shader_program const & shader, const char * uniform_block_name );

private:
    void init() {
        bind();
        bind_to_ibp();
    }
};

//required version of opengl: 4.3
class SSBO_base: public Ibp_buffer{
public:
    SSBO_base(GLuint a_binding_point, GLenum a_usage = GL_STREAM_DRAW):
        Ibp_buffer(GL_SHADER_STORAGE_BUFFER,a_binding_point,a_usage)
    {
        init();
    }

    SSBO_base(GLuint a_binding_point, Buffer_storage& storage):
        Ibp_buffer(GL_SHADER_STORAGE_BUFFER,a_binding_point,storage)
    {
        init();
    }

    void set_binding(Shader_program const & shader, const char * name );

private:
    void init() {
        bind();
        bind_to_ibp();
    }
};

} // namespace fgs
