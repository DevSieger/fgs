/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/common.hpp>
#include <type_traits>

namespace fgs {

class Texture;

/// The frame buffer object wrapper.
class Frame_buffer
{
public:
    Frame_buffer();
    ~Frame_buffer();

    Frame_buffer(Frame_buffer&&) = delete;

    GLuint id()const{
        return id_;
    }

    void bind(GLenum mode = GL_DRAW_FRAMEBUFFER)const;

    /// \brief Tests if the framebuffer object comlete.
    ///
    /// It breaks the GL_DRAW_FRAMEBUFFER binding.
    /// If the framebuffer object incomplete, the error is written to the log.
    bool check()const;

    /*!
     * \brief Attaches the given texture as the render target for the color output.
     * \param tx                        The given texture.
     * \param attachment_index          The index of the color attachment point.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_color(const Texture& tx, int attachment_index,GLint level=0);

    /*!
     * \brief Attaches the given texture as the render target for the depth output.
     * \param tx                        The given texture.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_depth(const Texture& tx, GLint level=0);

    /*!
     * \brief Attaches the given texture as the render target for the stencil output.
     * \param tx                        The given texture.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_stencil(const Texture& tx, GLint level=0);

    /*!
     * \brief Attaches the given texture as the render target for the depth and stencil output
     * \param tx                        The given texture.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_to_depth_stencil(const Texture& tx, GLint level=0);

    /*!
     * \brief Attaches the specified layer of the given texture as the render target for the color output.
     * \param tx                        The given texture.
     * \param attachment_index          The index of the color attachment point.
     * \param layer                     The index of the specified layer.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_layer_to_color(const Texture& tx, int attachment_index,GLint layer,GLint level=0);

    /*!
     * \brief Attaches the specified layer of the given texture as the render target for the depth output.
     * \param tx                        The given texture.
     * \param layer                     The index of the specified layer.
     * \param level                     The mipmap level of the given texture to attach.
     * \return                          true on success.
     *
     * It breaks the GL_DRAW_FRAMEBUFFER binding.
     */
    bool attach_layer_to_depth(const Texture& tx,GLint layer, GLint level=0);

    /// Returns the default framebuffer.
    static Frame_buffer& default_fbo();

    /*!
     * \brief Defines destionations to which fragment colors are written.
     * \param ix    the index of destionation attachment point.
     *
     * map_attachments(a, b, ...) maps the color zero to attachment point "a", e.t.c.
     * if some output should be ignored use value -1
     */
    template<typename ... Index>
    std::enable_if_t<std::conjunction_v< std::is_convertible<Index,int>... >>
    map_attachments(Index ... ix){
        auto get_attachment = [](int i) -> GLenum {
            return (i!=-1)? GL_COLOR_ATTACHMENT0 + i : GL_NONE;
        };
        GLenum ics[] = { get_attachment(ix) ...};
        map_attachments_raw(ics,sizeof...(ix));
    }


    /// Handles frame buffer bindings and calls glDrawBuffers(count,indices)
    void map_attachments_raw(GLenum* indices, GLsizei count);

private:
    Frame_buffer(GLuint id_);
    void init();

    GLuint id_;
};

} // namespace fgs
