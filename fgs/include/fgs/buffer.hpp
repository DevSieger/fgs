/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/common.hpp>
#include <fgs/basic_buffer.hpp>
#include <vector>

namespace fgs {

namespace dtl {

template<typename Prefix_data_type,typename Array_element_type>
class Buffer_content;

template<typename Array_element_type>
class Buffer_content<void, Array_element_type>{
public:
    using Array_element = Array_element_type;
    using Array_data    = std::vector<Array_element>;

    Array_data& array_data() {
        return array_data_;
    }

    const Array_data& array_data() const {
        return array_data_;
    }

    GLsizeiptr size() const {
        return array_data_.size() * sizeof(Array_element);
    }

protected:
    void upload_content(Buffer & buffer,size_t offset=0){
        if(!array_data().empty()){
            buffer.storage().upload(offset,array_data().data(),array_data().size());
        };
    }

private:
    Array_data array_data_;
};

template<typename Prefix_data_type>
class Buffer_content<Prefix_data_type,void>{
public:
    using Prefix_data   = Prefix_data_type;

    const Prefix_data& prefix_data() const {
        return prefix_data_;
    }
    Prefix_data& prefix_data() {
        return prefix_data_;
    }

    GLsizeiptr size() const {
        return sizeof(Prefix_data);
    }

protected:
    void upload_content(Buffer & buffer,size_t offset=0) {
        buffer.storage().upload(offset,prefix_data());
    }

private:
    Prefix_data prefix_data_;
};

template<typename Prefix_data_type,typename Array_element_type>
class Buffer_content: Buffer_content<Prefix_data_type,void>,Buffer_content<void, Array_element_type>{
    using Prefix_base   = Buffer_content<Prefix_data_type,void>;
    using Array_base    = Buffer_content<void, Array_element_type>;
public:
    using Prefix_data   = Prefix_data_type;
    using Array_element = Array_element_type;
    using Array_data    = std::vector<Array_element>;

    using Prefix_base::prefix_data;
    using Array_base::array_data;

    GLsizeiptr size() const {
        return Prefix_base::size()+Array_base::size();
    }

protected:
    void upload_content(Buffer & buffer,size_t offset=0) {
        Prefix_base::upload_content(buffer,offset);
        Array_base::upload_content(buffer,Prefix_base::size()+offset);
    }
};

} // namespace dtl

template<typename Base_tp,typename Prefix_data_type,typename Array_data_type>
class Buffer_with_content_interface: public Base_tp, public  dtl::Buffer_content<Prefix_data_type,Array_data_type>{
    using Content       = dtl::Buffer_content<Prefix_data_type,Array_data_type>;
public:
    using Basic_buffer  = Base_tp;
    using Basic_buffer::Basic_buffer;

    void commit(){
        Basic_buffer::storage().reallocate(Content::size());
        Content::upload_content(*this,0);
    }

private:
};

template<typename Prefix_data_type, typename Array_data_type>
using SSBO  = Buffer_with_content_interface<SSBO_base, Prefix_data_type, Array_data_type>;

template<typename Prefix_data_type, typename Array_data_type = void>
using UBO   = Buffer_with_content_interface<UBO_base, Prefix_data_type, Array_data_type>;

} // namespace fgs
