/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <string_view>
#include <string>
#include <fgs/common.hpp>
#include <dk/format_string.hpp>
#include <memory>

namespace fgs {

class Shader{
public:
    using Shader_source = std::string;

    // `compatibility` means the compatibility profile
    // sh_loader is used to cache repeated file loads.
    // a_type is GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, etc.
    Shader(GLenum a_type, std::string_view a_name, unsigned glsl_version, bool compatibility = false);

    // do not call this method twice with the same name.
    template<typename T>
    Shader& define(std::string_view name, T&& value){
        return add_source(dk::fstr("#define %1% %2%",name, std::forward<T>(value)));
    }

    Shader& add_source(std::string_view src);
    Shader& add_source(Shader_source&& src);

    bool compile();
    void clear();

    Shader_source get_source() const;

    GLenum              type()  const;
    const std::string&  name()  const;
    GLuint              id()    const;

    // makes "other" invalid. it is UB to call any method of an invalid object
    Shader(Shader&& other);

    ~Shader();
private:
    struct Data;

    Data& data(){ return *m_;}
    const Data& data() const { return *m_;}

    std::unique_ptr<Data> m_;
};

class Shader_program
{
public:
    Shader_program(std::string_view name_arg);

    // makes "other" invalid. it is UB to call any method of an invalid object
    Shader_program(Shader_program&& other);

    GLuint id() const;

    const std::string& name() const;

    void use();
    void recreate();

    // detaches all shaders
    bool link();

    Shader_program& attach(const Shader& shader);

    // Just a wrappers for glProgramUniform1* + glGetUniformLocation.
    bool set_uniform_u(const char * uniform_name, GLuint value);
    bool set_uniform_f(const char * uniform_name, GLfloat value);

    // only this variant should be used to set samplers.
    bool set_uniform_i(const char * uniform_name, GLint value);

    bool set_uniform_f(const char * uniform_name, GLfloat x, GLfloat y);
    bool set_uniform_f(const char * uniform_name, GLfloat x, GLfloat y, GLfloat z);
    bool set_uniform_f(const char * uniform_name, GLfloat x, GLfloat y, GLfloat z, GLfloat w);

    bool set_uniform_u(const char * uniform_name, GLuint x, GLuint y);
    bool set_uniform_u(const char * uniform_name, GLuint x, GLuint y, GLuint z);
    bool set_uniform_u(const char * uniform_name, GLuint x, GLuint y, GLuint z, GLuint w);

    bool set_uniform_i(const char * uniform_name, GLint x, GLint y);
    bool set_uniform_i(const char * uniform_name, GLint x, GLint y, GLint z);
    bool set_uniform_i(const char * uniform_name, GLint x, GLint y, GLint z, GLint w);

    ~Shader_program();
private:
    struct Data;

    Data&       data()          { return *m_; }
    const Data& data() const    { return *m_; }

    std::unique_ptr<Data> m_;
};
} // namespace fgs
