/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/common.hpp>
#include <memory>
#include <string_view>
#include <string>

namespace fgs {

class Performance_counter{
public:
    /// An object that represents the section of the gpu work to measure
    class Section{
    public:
        /*!
         * \brief Starts the section for the given counter
         * \param counter   the given counter
         * \param enabled   if this flag is set to false, no measurements are performed.
         *
         * Starts counting time spended to complete OGL commands initiated at the lifetime of the section.
         * DO NOT INTERSECT SECTIONS!!!
         */
        Section(Performance_counter& counter, bool enabled = true): cntr_(enabled ? &counter: nullptr){
            if(cntr_)   cntr_->section_begin();
        }
        Section(Section&& other): cntr_(other.cntr_){
            other.cntr_ = nullptr;
        }
        /// Ends the section and updates the counter, if the section was enabled.
        ~Section(){
            if(cntr_){
                cntr_->section_end();
            }
        }
    private:
        Performance_counter* cntr_ = nullptr;
    };

    using Time = double;
    using Name = std::string;

    /// Clears the accumulated results.
    void clear();

    /// Gets the mean time of the measured section execution on the GPU.
    Time get() const;

    const Name& name() const;

    Performance_counter(std::string_view section_name);
    ~Performance_counter();

    // invalidates other
    Performance_counter(Performance_counter&& other);

private:
    /// Starts the measured section.
    void section_begin();

    /// Ends the measured section.
    void section_end();

    bool valid() const{
        return static_cast<bool>(m_);
    }

    struct Data;

    Data& data(){ return *m_;}
    const Data& data() const { return *m_;}

    std::unique_ptr<Data> m_;
};

} // namespace fgs
