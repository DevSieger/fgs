/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/common.hpp>
#include <vector>

namespace fgs {

/*!
 * \brief An utility class for handling the vertex format.
 *
 * It is the wrapper around glVertexAttribPointer.
 * The vertex attribute data is assumed to be interleaved in the VBO.
 */
class Vertex_format{
public:
    enum class Type{
        FLOAT,
        INTEGER,
        NORMALIZED,                         // The floating point type with normalized values.
        DOUBLE
    };

    /// Adds the vertex attribute with the specified parameters.
    Vertex_format & add(Type result_type,GLuint index,GLuint size,GLenum type,size_t offset);

    // 'add' using result_type == FLOAT
    Vertex_format & add_f(GLuint index,GLuint size,GLenum type,size_t offset);
    // 'add' using result_type == INTEGER
    Vertex_format & add_i(GLuint index,GLuint size,GLenum type,size_t offset);
    // 'add' using result_type == NORMALIZED
    Vertex_format & add_n(GLuint index,GLuint size,GLenum type,size_t offset);
    // 'add' using result_type == DOUBLE
    Vertex_format & add_d(GLuint index,GLuint size,GLenum type,size_t offset);

    Vertex_format& set_stride(GLuint stride_a){
        stride = stride_a;
        return *this;
    }

    // To use with the interleaved attribute data.
    /// Sets the size of the data of single vertex to the size of the given type.
    template<typename T>
    Vertex_format & set_stride(){
        return set_stride(sizeof(T));
    }

    /*!
     * \brief Applies the vertex format to the given VBO.
     * \param vbo                       An id of the given VBO.
     * \param instancing_divisor        See glVertexAttribDivisor (https://www.opengl.org/wiki/GLAPI/glVertexAttribDivisor).
     * \param offset                    The offset of the vertex data in the given VBO.
     *
     * instancing_divisor is applied to all vertex attributes.
     * Binds the given VBO to GL_ARRAY_BUFFER.
     */
    void apply(GLuint vbo, GLuint instancing_divisor, size_t offset=0);

    /// Disables the vertex attributes used in the format.
    void disable();

private:
    struct Attribute_format{
        /// The vertex attribute type (in shader).
        Type result_type;

        /// The vertex attribute index.
        GLuint index;

        /// The number of components.
        GLuint size;

        /// \brief The data type for storing the component in the VBO.
        /// \details e.g. GL_BYTE, GL_UNSIGNED_INT ( the type parameter of glVertexAttrib[I/L]Pointer ).
        GLenum type;

        /// The offset of the vertex attribute in the data store of the VBO.
        size_t offset;
    };

    using Attributes = std::vector<Attribute_format>;
    Attributes  attributes;
    GLuint      stride;
};

} // namespace fgs
