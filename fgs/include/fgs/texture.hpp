/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#pragma once
#include <fgs/common.hpp>
#include <list>
#include <vector>
#include <variant>
#include <initializer_list>

namespace fgs {

/// The base class for all kinds of textures.
class Texture
{
public:
    GLuint id() const{
        return id_;
    }

    void bind() const{
        glBindTexture(target_,id_);
    }

    void update_mipmaps();                                                          // also binds the texture.

    /*!
     * \brief Binds the texture to the given texture unit.
     * \param texture_unit          The given texture unit. 0 is invalid value.
     * \details The zero texture unit is reserved to use with generic bindings.
     *
     * If the binding to the texture unit exists, then no bindings performed.
     */
    void bind_to_texture_unit(GLuint texture_unit);

    /*!
     * \brief Binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     * \param format                The format of storing data into the image from shaders   (e.g. GL_RGBA8, GL_R32F).
     *
     * This method don't set the format of the texture.
     * If the binding to the specified image unit with the same parameters exists, then no bindings performed.
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit( GLuint unit, GLenum access, GLint level, GLenum format );

    /*!
     * \brief Binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     *
     * The format of storing data into the image from shaders is the internal format that was used to define the storage of the texture (e.g. see Texture_2D::storage).
     * If the binding to the specified image unit with the same parameters exists, then no bindings performed (including the internal format currently used).
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit( GLuint unit,GLenum access,GLint level=0 );


    /*!
     * \brief Breaks the binding of the texture with the given image unit.
     * \param unit                  The image unit.
     *
     * If the texture isn't bound to the given image unit, then no actions performed.
     */
    void break_image_unit_binding(GLuint unit);

    /*!
     * \brief Breaks the binding of the texture with the given texture unit.
     * \param unit                  The texture unit.
     *
     * If the texture isn't bound to the given texture unit, then no actions performed.
     */
    void break_texture_unit_binding(GLuint unit);

    /*!
    * \brief Sets the texture filtering functions to the given one.
    * \param min_filter             The given texture minifying function (e.g. GL_LINEAR_MIPMAP_LINEAR) (see GL_TEXTURE_MIN_FILTER).
    * \param mag_filter             The given texture magnification function ( GL_LINEAR or GL_NEAREST) (see GL_TEXTURE_MAG_FILTER).
    * \details Binds the texture.
    */
    void filter(GLint min_filter, GLint mag_filter);

    /// \brief Sets the maximum degree of anisotropy (i.e. GL_TEXTURE_MAX_ANISOTROPY_EXT)
    /// \details Binds the texture if the EXT_texture_filter_anisotropic extension is available, otherwise the call is ignored.
    void anisotropy(float value);

    /// Returns an internal format of the texture (e.g. GL_RGBA8) if it is defined.
    GLenum format() const{
        return format_;
    }

    /// Returns the number of mipmap levels.
    unsigned levels() const{
        return levels_;
    }

    /*!
     * \brief Temporarily binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     * \param format                The format of storing data into the image from shaders   (e.g. GL_RGBA8, GL_R32F).
     *
     * This method perform only opengl binding of the texture. The binding will not been restored on the texture recreation.
     * This method don't set the format of the texture.
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit_once(GLuint unit,GLenum access,GLint level,GLenum format) const;

    /*!
     * \brief Temporarily binds the given level of the texture to the specified image unit.
     * \param unit                  The index of the image unit.
     * \param access                The type of the access to be performed by shaders   (i.e. GL_READ_ONLY, GL_WRITE_ONLY, GL_READ_WRITE).
     * \param level                 The level of the texture.
     *
     * This method perform only opengl binding of the texture. The binding will not been restored on the texture recreation.
     * The format of storing data into the image from shaders is the internal format that was used to define the storage of the texture (e.g. see Texture_2D::storage).
     * See the documentation of glBindImageTexture (e.g. https://www.opengl.org/wiki/GLAPI/glBindImageTexture ).
     */
    void bind_to_image_unit_once(GLuint unit,GLenum access,GLint level=0) const;

    /*!
     * \brief Temporarily binds the texture to the given texture unit.
     * \param texture_unit          The given texture unit. 0 is invalid value.
     *
     * This method perform only opengl binding of the texture. The binding will not been restored on the texture recreation.
     * The zero texture unit is reserved to use with generic bindings.
     * If the binding to the texture unit exists, then no bindings performed.
     */
    void bind_to_texture_unit_once(GLuint texture_unit) const;

    /*!
     * \brief constructs texture for the given target
     * \param target_a  The target to which texture is bound (e.g. GL_TEXTURE_2D).
     */
    Texture(GLenum target_a);

    struct Parameter{
        using Value = std::variant<GLint,GLfloat,GLint*,GLfloat*>;
        GLenum  name;
        Value   value;
    };

    /// Set the given parameter to the specified value
    ///
    /// Parameter value will be set again on recreation of the ogl texture (e.g. after call of storage method)
    void set_parameter(GLenum name, GLint value){
        set_parameter(Parameter{name,value});
    }
    void set_parameter(GLenum name, GLfloat value){
        set_parameter(Parameter{name,value});
    }
    void set_parameter(GLenum name, GLint* value){
        set_parameter(Parameter{name,value});
    }
    void set_parameter(GLenum name, GLfloat* value){
        set_parameter(Parameter{name,value});
    }
    void set_parameter(std::initializer_list<Parameter> il);

    // it is only one pointer for a safety.
    virtual ~Texture();

protected:
    //methods:
    Texture(Texture&& source);
    Texture() = default;
    /// Rebinds the texture to the previously used image & texture units.
    void rebind_to_units();

    /// \brief Deletes the underlying texture object and creates a new one.
    /// \details It binds the texture. Restores texure parameters that have been set.
    void recreate();

    /// \brief Deletes the underlying texture object and creates a new one.
    /// \details Doesn't bind the texture.
    void raw_recreate();

    /// Restores parameters been set
    void restore_parameters();

    //data fields
    /// The target to which texture is bound (e.g. GL_TEXTURE_2D).
    GLenum      target_;        ///< \details the value is defined by the child class.
    unsigned    levels_ = 0;    ///< the number of mipmap levels.
    GLenum      format_;        ///< The internal format of the texture, e.g. GL_RGBA8.

private:
    // doesn't store parameter if an ogl error happen
    void set_parameter(const Parameter& p, bool no_bind = false );

    // performs glTexParameter* call, doesn't store parameter for "restore"
    // return false if an ogl error happen.
    bool set_parameter_once(const Parameter& p, bool no_bind = false );

    using Unit=GLuint;

    // the number of possible parameters is VERY limited, using of std::map isn't necessary
    using Parameters = std::vector<Parameter>;

    GLuint id_  = 0;

    /*
     * rebind_to_units is mostly used to reestablish the bindings of the recreated texture.
     * However, when the old texture object is deleted, the associated image binding states are cleared,
     * so we can't restore them using only the OpenGL API.
     * This structure is used to save them.
     */
    struct Image_binding{
        GLuint  unit;
        GLenum  access;
        GLint   level;
        GLenum  format;
    };

    std::list<Image_binding>    image_bindings_;
    std::list<Unit>             texture_bindings_;

    // Texture parameters to restore after a recreation
    Parameters                  params_;
};

class Texture_2D:public Texture
{
public:
    Texture_2D();

    using Size = unsigned;

    Size width() const {
        return size_[0];
    }

    Size height() const {
        return size_[1];
    }

    /*!
     * \brief (Re)initializes the texture, and specifies its storage using the given parameters.
     * \param tx_width          The width of the texture, in texels.
     * \param tx_height         The height ot the texture, in texels.
     * \param levels            The number of the mipmap levels.
     * \param format            The inner format of the texture (the sized one, see glTexStorage2D, https://www.opengl.org/wiki/GLAPI/glTexStorage2D).
     * \return                  true on success.
     *
     * The immutable storage is used, so the texture should be recreated, i.e. the texture id is changed.
     * The bindings to the texture/image units are restored for the recreated texture.
     * It binds the texture.
     */
    bool storage(int tx_width, int tx_height, int levels, int format=GL_RGBA8);

private:
    Size size_[2];
};

class Texture_3D: public Texture
{
public:
    Texture_3D();
    using Size = unsigned;

    Size width() const {
        return size_[0];
    }

    Size height() const {
        return size_[1];
    }

    Size depth() const {
        return size_[2];
    }

    /*!
     * \brief (Re)initializes the texture, and specifies its storage using the given parameters.
     * \param tx_width                  The width of the texture, in texels.
     * \param tx_height                 The height ot the texture, in texels.
     * \param tx_depth                  The texture depth, in texels, or the number of textures in the texture array.
     * \param levels                    The number of the mipmap levels.
     * \param format                    The inner format of the texture (the sized one, see glTexStorage3D, https://www.opengl.org/wiki/GLAPI/glTexStorage3D).
     * \return                          true on success.
     *
     * The immutable storage is used, so the texture should be recreated, i.e. the texture id is changed.
     * The bindings to the texture/image units are restored for the recreated texture.
     * It binds the texture.
     */
    bool storage(int tx_width, int tx_height, int tx_depth, int levels, int format=GL_RGBA8);
private:
    Size size_[3];
};

class Texture_array : public Texture_3D
{
public:
    Texture_array();

    /*!
     * \brief Sets the content of the specified index of the texture.
     * \param index         The specified index.
     * \param pixel_fmt     A format of a pixel (e.g. GL_RGBA)
     * \param pixel_type    A data type of a pixel (e.g. GL_UNSIGNED_BYTE, GL_FLOAT)
     * \param data          A content to upload
     *
     * a size of the data is defined by the size of the texture storage (see Texture_3D::storage)
     * It binds the texture array.
     */
    void load(unsigned int index, GLenum pixel_fmt, GLenum pixel_type, const void* data );
};

class Cube_map: public Texture_2D
{
public:
    Cube_map();

    /*!
     * \brief Sets the content of the specified face.
     * \param face          The specified face (e.g. GL_TEXTURE_CUBE_MAP_POSITIVE_X)
     * \param pixel_fmt     A format of a pixel (e.g. GL_RGBA)
     * \param pixel_type    A data type of a pixel (e.g. GL_UNSIGNED_BYTE, GL_FLOAT)
     * \param data          A pixel data to upload
     *
     * a size of the data is defined by the size of the texture storage (see Texture_2D::storage)
     * It binds the cube map.
     */
    void load(GLenum face, GLenum pixel_fmt, GLenum pixel_type, const void* data);
};

class Texture_view: public Texture{
public:
    /// Constructs new texture view of the specifed type of target
    Texture_view(GLenum target);

    /*!
     * \brief Links the texture view to the given texture using the specified parameters
     * \param tx                The texure to link that is used as a source.
     * \param frmt              The internal format for a data interpretation( e.g. GL_RGBA8 )
     * \param lvl_offset        The minimal mip map level of tx to use in the texture view
     * \param num_lvls          The number of the mip map levels of tx to use
     * \param layer_offset_p    The minimal layer of tx to use in the texture view
     * \param n_layers          The number of layers of tx to use
     *
     * See glTextureView.
     *
     * If the texture view has been linked before, it is recreated and
     * the old texture parameters are restored, see Tx_parameters.
     */
    void link(const Texture& tx, GLenum frmt, GLuint lvl_offset, GLuint num_lvls, GLuint layer_offset_p, GLuint n_layers);

    /// Returns the last texture linked to this one as the source
    const Texture* source_tx(){
        return source_tx_;
    }

    unsigned level_offset() const   { return lvl_offset_;};
    unsigned layer_offset() const   { return layer_offset_;};
    unsigned num_layers()   const   { return num_layers_;};

private:
    const Texture* source_tx_   = nullptr;
    unsigned lvl_offset_        = 0;
    unsigned layer_offset_      = 0;
    unsigned num_layers_        = 0;
};

} // namespace fgs
