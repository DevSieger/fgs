/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/performance_counter.hpp>
#include <cstdint>
#include <cassert>

namespace fgs {

using u32 = std::uint32_t;
using u64 = std::uint64_t;

namespace  {

/// An utility class, wraps ogl 'GL_TIME_ELAPSED' query object.
class Query{
public:

    Query();

    ~Query();

    // DO NOT INTERSECT SECTIONS OF DIFFERENT QUERIES !
    void section_begin();
    void section_end();

    // returns elapsed time, in nanoseconds.
    // blocking call.
    u32 get_value() const;

private:
    GLuint id_  = 0;
};

Query::Query()
{
    glGenQueries(1,&id_);
}

Query::~Query()
{
    if(id_)
        glDeleteQueries(1,&id_);
}

void Query::section_begin()
{
    glBeginQuery(GL_TIME_ELAPSED,id_);
    check_gl_error();
}

void Query::section_end()
{
    glEndQuery(GL_TIME_ELAPSED);
    check_gl_error();
}

u32 Query::get_value() const
{
    u32 value;
    glGetQueryObjectuiv(id_, GL_QUERY_RESULT , &value);
    check_gl_error();
    return value;
}

}

struct Performance_counter::Data{
    Data(std::string_view sv): name(sv){}
    Name name;
    Time mean_          = 0;
    u64 sample_count_   = 0;
    Query query_a_;
    Query query_b_;
    Query * current_    = nullptr;
};


void Performance_counter::clear()
{
    assert(valid());
    data().mean_           = 0;
    data().sample_count_   = 0;
}

Performance_counter::Time Performance_counter::get() const{
    assert(valid());
    return data().mean_;
}

void Performance_counter::section_begin()
{
    assert(valid());
    auto& curr = data().current_;
    curr = (curr == &data().query_b_)? &data().query_a_ : &data().query_b_;

    auto nanosec = curr->get_value();
    Time sec    = nanosec * 0.000000001;
    data().mean_       = (data().mean_ * data().sample_count_  + sec)/(data().sample_count_+1);
    ++ data().sample_count_;

    curr->section_begin();
}

void Performance_counter::section_end()
{
    assert(valid());
    data().current_->section_end();
}

const Performance_counter::Name& Performance_counter::name() const
{
    assert(valid());
    return data().name;
}

Performance_counter::Performance_counter(std::string_view section_name)
{
    m_ = std::make_unique<Data>(section_name);

    // to simplify #section_begin method.
    data().query_a_.section_begin();
    data().query_a_.section_end();
    data().query_b_.section_begin();
    data().query_b_.section_end();
    data().current_ = &data().query_a_;
}

Performance_counter::~Performance_counter() = default;
Performance_counter::Performance_counter(Performance_counter&& other) = default;

} // namespace fgs
