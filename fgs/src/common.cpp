/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/common.hpp>
#include <unordered_set>
#include <utility>
#include <string>

#include <dk/logging.hpp>

namespace fgs {

namespace dtl {

// from exge211, optimized
bool check_gl_error_f(const char * file, int line){
    using String = std::string;
    GLenum err;
    bool errors = false;
    static std::unordered_set<String> reported;  // to prevent spamming.
    while( (err=glGetError()) != GL_NO_ERROR ){
        // is there a guarantee that "file" pointer will be the same for the given file & line every time?
        String error_key = dk::fstr("%1%, %2%:%3%",file,line,err);
        if(!reported.count(error_key)){
            reported.emplace(std::move(error_key));

            String error_type;
            switch(err){
                case GL_INVALID_ENUM:                   error_type = "INVALID_ENUM"; break;
                case GL_INVALID_VALUE:                  error_type = "INVALID_VALUE"; break;
                case GL_INVALID_OPERATION:              error_type = "INVALID_OPERATION"; break;
                case GL_INVALID_FRAMEBUFFER_OPERATION:  error_type = "INVALID_FRAMEBUFFER_OPERATION"; break;
                case GL_OUT_OF_MEMORY:                  error_type = "OUT_OF_MEMORY"; break;
                case GL_STACK_UNDERFLOW:                error_type = "STACK_UNDERFLOW"; break;
                case GL_STACK_OVERFLOW:                 error_type = "STACK_OVERFLOW"; break;

                default:                                error_type = dk::fstr("\"%1%\"",(unsigned)err); break;
            }

            dk::log(dk::Detail_level::CRITICAL,"gl error %1% at %3% line %2%",error_type,line,file);
        }
        errors=true;
    }
    return errors;
}


} // namespace dtl

bool initialize_ogl_fn(){
    int res = ogl_LoadFunctions();
    return res==ogl_LOAD_SUCCEEDED;
}

} // namespace fgs
