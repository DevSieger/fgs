/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/shader.hpp>
#include <vector>
#include <dk/logging.hpp>

namespace fgs {

struct Shader::Data{
    Data(GLenum a_type, std::string_view a_name, std::string&& a_version_str):
        name(a_name),version_str(std::move(a_version_str)), type(a_type)
    {}

    std::vector<Shader_source> sources;
    const std::string name;
    const std::string version_str;
    GLuint id  = 0;
    const GLenum type;
};

Shader::Shader(GLenum a_type, std::string_view a_name, unsigned glsl_version, bool compatibility){
    std::string version_str = dk::fstr("#version %1% %2%\n", glsl_version, (compatibility?"compatibility":"core") );
    m_ = std::make_unique<Data>(a_type,a_name,std::move(version_str));
}

Shader& Shader::add_source(std::string_view src){
    auto& srcs = data().sources;
    if(!src.empty()){
        srcs.emplace_back(src);
        srcs.back().push_back('\n');
    }
    return *this;
}

Shader& Shader::add_source(Shader::Shader_source&& src)
{
    auto& srcs = data().sources;
    if(!src.empty()){
        srcs.emplace_back(std::move(src));
        srcs.back().push_back('\n');
    }
    return *this;
}

bool Shader::compile()
{
    GLint res=GL_FALSE;
    auto& srcs  = data().sources;
    if(srcs.size()){
        glDeleteShader(id());
        data().id = glCreateShader(data().type);
        std::vector<const char*> source_list;
        source_list.reserve(srcs.size()+1);
        std::vector<GLint> source_sizes;
        source_sizes.reserve(srcs.size()+1);

        source_list.push_back(data().version_str.data());
        source_sizes.push_back(data().version_str.size());

        for(auto& src: srcs  ){
            source_list.push_back(src.data());
            source_sizes.push_back(src.size());
        };

        glShaderSource(id(),source_list.size(),source_list.data(),source_sizes.data());
        glCompileShader(id());
        int log_len=0;
        glGetShaderiv(id(),GL_COMPILE_STATUS,&res);
        glGetShaderiv(id(),GL_INFO_LOG_LENGTH,&log_len);
        if(res!=GL_TRUE){
            dk::log(dk::Detail_level::CRITICAL,"failed to compile shader %1%",name());
        }
        if(log_len>1){
            std::vector<char> log(log_len);
            glGetShaderInfoLog(id(),log_len,0,&log[0]);
            dk::log(dk::Detail_level::DEBUG,"Shader %1% compilation log: %2%",name(), &log[0]);
        }
    }else{
        dk::log(dk::Detail_level::CRITICAL,"shader %1% is empty",name());
    }
    return res;
}

void Shader::clear()
{
    data().sources.clear();
    glDeleteShader(id());
    data().id = 0;
}

Shader::Shader_source Shader::get_source() const
{
    Shader_source ret = data().version_str;
    for(auto& src: data().sources){
        ret+=src;
    }
    return ret;
}

GLenum Shader::type() const   { return data().type;}

const std::string&Shader::name() const   { return data().name;}

GLuint Shader::id() const   { return data().id;}

Shader::Shader(Shader&& other):
    m_(std::move(other.m_))
{}

Shader::~Shader()
{
    if(m_) clear();
}


//================= Shader_program =================//

struct Shader_program::Data{
    Data (std::string_view sv, GLuint id_a): name(sv),id(id_a) {}

    std::string         name;
    GLuint              id = 0;
    std::vector<GLuint> shaders;
};

Shader_program::Shader_program(std::string_view name_arg){
    GLuint pr_id = glCreateProgram();
    m_ = std::make_unique<Data>(name_arg,pr_id);
}

Shader_program::Shader_program(Shader_program&& other) = default;


Shader_program::~Shader_program()
{
    if(m_) glDeleteProgram(id());
}

GLuint Shader_program::id() const {
    return data().id;
}

const std::string& Shader_program::name() const {
    return data().name;
}

void Shader_program::use()
{
    glUseProgram(id());
    check_gl_error();
}

void Shader_program::recreate()
{
    data().shaders.clear();
    glDeleteProgram(id());
    data().id = glCreateProgram();
}

bool Shader_program::link()
{
    glLinkProgram(id());
    check_gl_error();
    GLint res=GL_FALSE;
    int log_len=0;
    glGetProgramiv(id(),GL_LINK_STATUS,&res);
    glGetProgramiv(id(),GL_INFO_LOG_LENGTH,&log_len);
    if(res!=GL_TRUE){
        dk::log(dk::Detail_level::CRITICAL,"failed to link shaders for %1%",name());
    }
    if(log_len>1){
        std::vector<char> log(log_len);
        glGetProgramInfoLog(id(),log_len,0,&log[0]);
        dk::log(dk::Detail_level::DEBUG,&log[0]);
    }
    auto& shdrs = data().shaders;
    for(GLuint& sh_id: shdrs){
        glDetachShader(id(),sh_id);
    }
    shdrs.clear();
    return res;
}

Shader_program& Shader_program::attach(const Shader& shader)
{
    glAttachShader(id(),shader.id());
    data().shaders.push_back(shader.id());
    check_gl_error();
    return *this;
}

namespace {

template<typename Funct, typename ... Value>
bool set_uniform(Shader_program& s_prog, Funct funct, const char * unif_name, Value ... value ){
    GLint location  = glGetUniformLocation(s_prog.id(), unif_name);
    check_gl_error();
    if(location!=-1){
        funct(s_prog.id(), location, value...);
        return check_gl_error();
    }else{
        dk::log(dk::Detail_level::NON_CRITICAL,"can't find the uniform %1% in %2%",unif_name,s_prog.name());
    }
    return false;
}

}

bool Shader_program::set_uniform_i(const char * uniform_name, GLint value)
{
    return set_uniform(*this, glProgramUniform1i, uniform_name, value);
}

bool Shader_program::set_uniform_u(const char * uniform_name, GLuint value)
{
    return set_uniform(*this, glProgramUniform1ui, uniform_name, value);
}

bool Shader_program::set_uniform_f(const char * uniform_name, GLfloat value)
{
    return set_uniform(*this, glProgramUniform1f, uniform_name, value);
}

bool Shader_program::set_uniform_f(const char* uniform_name, GLfloat x, GLfloat y)
{
    return set_uniform(*this, glProgramUniform2f, uniform_name, x, y);
}

bool Shader_program::set_uniform_f(const char* uniform_name, GLfloat x, GLfloat y, GLfloat z)
{
    return set_uniform(*this, glProgramUniform3f, uniform_name, x, y, z);
}

bool Shader_program::set_uniform_f(const char* uniform_name, GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
    return set_uniform(*this, glProgramUniform4f, uniform_name, x, y, z, w);
}

bool Shader_program::set_uniform_u(const char* uniform_name, GLuint x, GLuint y)
{
    return set_uniform(*this, glProgramUniform2ui, uniform_name, x,y);
}
bool Shader_program::set_uniform_u(const char* uniform_name, GLuint x, GLuint y, GLuint z)
{
    return set_uniform(*this, glProgramUniform3ui, uniform_name, x,y,z);
}
bool Shader_program::set_uniform_u(const char* uniform_name, GLuint x, GLuint y, GLuint z, GLuint w)
{
    return set_uniform(*this, glProgramUniform4ui, uniform_name, x,y,z,w);
}

bool Shader_program::set_uniform_i(const char* uniform_name, GLint x, GLint y)
{
    return set_uniform(*this, glProgramUniform2i, uniform_name, x,y);
}

bool Shader_program::set_uniform_i(const char* uniform_name, GLint x, GLint y, GLint z)
{
    return set_uniform(*this, glProgramUniform3i, uniform_name, x,y,z);
}

bool Shader_program::set_uniform_i(const char* uniform_name, GLint x, GLint y, GLint z, GLint w)
{
    return set_uniform(*this, glProgramUniform4i, uniform_name, x,y,z,w);
}

} // namespace fgs
