/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/vertex_format.hpp>

namespace fgs {

Vertex_format &Vertex_format::add(Type result_type, GLuint index, GLuint size, GLenum type, size_t offset)
{
    attributes.push_back({result_type,index,size,type,offset});
    return *this;
}

Vertex_format &Vertex_format::add_f(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Type::FLOAT,index,size,type,offset);
}

Vertex_format &Vertex_format::add_i(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Type::INTEGER,index,size,type,offset);
}

Vertex_format &Vertex_format::add_n(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Type::NORMALIZED,index,size,type,offset);
}

Vertex_format &Vertex_format::add_d(GLuint index, GLuint size, GLenum type, size_t offset)
{
    return add(Type::DOUBLE,index,size,type,offset);
}

void Vertex_format::apply(GLuint vbo, GLuint instancing_divisor, size_t offset){
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    for(Attribute_format & a: attributes ){
        glEnableVertexAttribArray(a.index);
        check_gl_error();
        switch(a.result_type){
            case Type::FLOAT:
                glVertexAttribPointer(a.index,a.size,a.type,GL_FALSE,stride,(GLvoid*)(a.offset+offset));
                break;
            case Type::NORMALIZED:
                glVertexAttribPointer(a.index,a.size,a.type,GL_TRUE,stride,(GLvoid*)(a.offset+offset));
                break;
            case Type::INTEGER:
                glVertexAttribIPointer(a.index,a.size,a.type,stride,(GLvoid*)(a.offset+offset));
                break;
            case Type::DOUBLE:
                glVertexAttribLPointer(a.index,a.size,a.type,stride,(GLvoid*)(a.offset+offset));
                break;
        }
        check_gl_error();
        glVertexAttribDivisor(a.index,instancing_divisor);
        check_gl_error();
    }
}

void Vertex_format::disable()
{
    for(Attribute_format& a: attributes ){
        glDisableVertexAttribArray(a.index);
        check_gl_error();
    }
}

} // namespace fgs
