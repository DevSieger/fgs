/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/texture.hpp>
#include <dk/logging.hpp>
#include <algorithm>

namespace fgs {

void Texture::update_mipmaps()
{
    if(levels_>1){
        bind();
        glGenerateMipmap(target_);
        check_gl_error();
    }
}

void Texture::bind_to_texture_unit(GLuint texture_unit)
{
    if(texture_unit<1){
        dk::log(dk::Detail_level::NON_CRITICAL,"do not bind to the texture unit 0! this unit is reserved.");
        return;
    }

    auto it=std::find(texture_bindings_.begin(),texture_bindings_.end(),texture_unit);
    if(it!=texture_bindings_.end()){
        return;
    }
    texture_bindings_.push_back(texture_unit);
    bind_to_texture_unit_once(texture_unit);
}

void Texture::bind_to_image_unit(GLuint unit, GLenum access, GLint level, GLenum format)
{
    // I don't like to call break_image_unit_binding, because break_image_unit_binding calls unnecessary glBindImageTexture.
    auto it=std::find_if(image_bindings_.begin(),image_bindings_.end(),[unit](Image_binding const& subject){ return subject.unit==unit;});
    if(it!=image_bindings_.end()){
        if(it->access==access && it->level==level && it->format==format){
            return;
        }
        image_bindings_.erase(it);
    }
    image_bindings_.emplace_back(Image_binding{unit,access,level,format});
    bind_to_image_unit_once(unit,access,level,format);
}

void Texture::bind_to_image_unit(GLuint unit, GLenum access, GLint level)
{
    bind_to_image_unit(unit,access,level,format_);
}

void Texture::break_image_unit_binding(GLuint unit)
{
    //It is guaranteed that there is only one binding to this image unit.
    auto it=std::find_if(image_bindings_.begin(),image_bindings_.end(),[unit](Image_binding const& subject){ return subject.unit==unit;});
    if(it!=image_bindings_.end()){
        it=image_bindings_.erase(it);
        glBindImageTexture(unit,0,0,GL_FALSE,0,GL_READ_ONLY,GL_R8);
    };
}

void Texture::break_texture_unit_binding(GLuint unit)
{
    //It is guaranteed that there is only one binding to this texture unit.
    auto it=std::find(texture_bindings_.begin(),texture_bindings_.end(),unit);
    if(it!=texture_bindings_.end()){
        texture_bindings_.erase(it);
        glActiveTexture(GL_TEXTURE0+unit);
        glBindTexture(target_,0);
        glActiveTexture(GL_TEXTURE0);
    };
}

void Texture::filter(GLint min_filter, GLint mag_filter)
{
    set_parameter({
                      {GL_TEXTURE_MIN_FILTER,min_filter},
                      {GL_TEXTURE_MAG_FILTER,mag_filter}
                  });
}

Texture::~Texture()
{
    if(id_){
        glDeleteTextures(1,&id_);
        id_=0;
    };
}

Texture::Texture(Texture&& source):
    target_(source.target_),
    levels_(source.levels_),
    format_(source.format_),
    id_(source.id_),
    image_bindings_(std::move(source.image_bindings_)),
    texture_bindings_(std::move(source.texture_bindings_))
{
    source.levels_  = 0;
    source.id_      = 0;
    source.image_bindings_.clear();
    source.texture_bindings_.clear();
}

void Texture::rebind_to_units()
{
    for(Image_binding const& binding: image_bindings_){
        glBindImageTexture(binding.unit,id_,binding.level,GL_TRUE,0,binding.access,binding.format);
        check_gl_error();
    }

    for(Unit texture_unit: texture_bindings_){
        glActiveTexture(GL_TEXTURE0+texture_unit);
        check_gl_error();
        bind();
    }
    glActiveTexture(GL_TEXTURE0);
}

void Texture::recreate()
{
    if(id_){
        raw_recreate();
        restore_parameters();
        // I'm not sure if it is correct to rebind the new texture without the defined storage to the image/texture units.
    }else{
        glGenTextures(1,&id_);
        bind();
    }
}

void Texture::raw_recreate()
{
    glDeleteTextures(1,&id_);
    glGenTextures(1,&id_);
}

void Texture::restore_parameters()
{
    for(auto& p : params_){
        set_parameter_once(p);
    }
}

void Texture::set_parameter(const Texture::Parameter& p, bool no_bind)
{
    if( !set_parameter_once(p,no_bind) ) return;

    auto it = std::find_if(params_.begin(),params_.end(),[&p](const Parameter& v){ return  v.name == p.name;});
    if(it==params_.end()){
        params_.emplace_back(p);
    }else{
        it->value = p.value;
    }
}

bool Texture::set_parameter_once(const Texture::Parameter& p, bool no_bind)
{
    class Visitor{
    public:
        Visitor(GLenum target, GLenum pname):
            tgt_(target), pname_(pname)
        {}

        void operator()(GLint v) const {
            glTexParameteri(tgt_,pname_,v);
        }
        void operator()(const GLint* v) const {
            glTexParameteriv(tgt_,pname_,v);
        }
        void operator()(GLfloat v) const {
            glTexParameterf(tgt_,pname_,v);
        }
        void operator()(const GLfloat* v) const {
            glTexParameterfv(tgt_,pname_,v);
        }

    private:
        GLenum tgt_, pname_;
    };

    if( id() ) {
        check_gl_error();   // flush errors
        if(! no_bind ) bind();
        std::visit(Visitor(target_,p.name), p.value);
        if(check_gl_error()) return false;
    }
    return true;
}

void Texture::set_parameter(std::initializer_list<Texture::Parameter> il)
{
    bind();
    for(const Parameter& p: il){
        set_parameter(p, true);
    }
}

void Texture::anisotropy(float value)
{
    if(ogl_ext_EXT_texture_filter_anisotropic){
        /* todo: this is so terrible!!! */
        float max_anisotropy;
        glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&max_anisotropy);
        if(max_anisotropy<value){
            value=max_anisotropy-0.01f;
        }
        bind();
        glTexParameterf(target_,GL_TEXTURE_MAX_ANISOTROPY_EXT,value);
        check_gl_error();
    };

}

void Texture::bind_to_image_unit_once(GLuint unit, GLenum access, GLint level, GLenum format) const
{
    glBindImageTexture(unit,id(),level,GL_TRUE,0,access,format);
    check_gl_error();
}

void Texture::bind_to_image_unit_once(GLuint unit, GLenum access, GLint level) const
{
    bind_to_image_unit_once(unit,access,level,format_);
}

void Texture::bind_to_texture_unit_once(GLuint texture_unit) const
{
    if(texture_unit<1){
        dk::log(dk::Detail_level::NON_CRITICAL,"do not bind to the texture unit 0! this unit is reserved.");
        return;
    }

    glActiveTexture(GL_TEXTURE0+texture_unit);
    check_gl_error();
    bind();
    glActiveTexture(GL_TEXTURE0);
}

Texture::Texture(GLenum target_a):
    target_(target_a)
{}

//======== Texture_2D ========//

Texture_2D::Texture_2D():
    Texture (GL_TEXTURE_2D)
{}

bool Texture_2D::storage(int tx_width, int tx_height,int levels,int format)
{
    recreate();
    bind();
    check_gl_error();
    levels_     = levels;
    format_     = format;
    size_[0]    = tx_width;
    size_[1]    = tx_height;
    glTexStorage2D( target_, levels, format, tx_width, tx_height );
    check_gl_error();
    rebind_to_units();
    if(check_gl_error()){
        return false;
    }else{
        return true;
    }
}

//======== Texture_3D ========//

Texture_3D::Texture_3D():
    Texture (GL_TEXTURE_3D)
{}

bool Texture_3D::storage(int tx_width, int tx_height, int tx_depth, int levels, int format)
{
    recreate();
    bind();
    check_gl_error();

    levels_     = levels;
    format_     = format;
    size_[0]    = tx_width;
    size_[1]    = tx_height;
    size_[2]    = tx_depth;

    glTexStorage3D( target_, levels, format, tx_width, tx_height, tx_depth);
    rebind_to_units();
    if(check_gl_error()){
        return false;
    }else{
        return true;
    }

}

//======== Texture_array ========//

Texture_array::Texture_array()
{
    target_=GL_TEXTURE_2D_ARRAY;
}

void Texture_array::load(unsigned int index, GLenum pixel_fmt, GLenum pixel_type, const void* data ){
    if(index>=depth()){
         dk::log(dk::Detail_level::CRITICAL,"requested to load index greater than size");
        return;
    }
    bind();

    glTexSubImage3D(target_, 0, 0, 0, index, width(), height() ,1, pixel_fmt, pixel_type, data);
    check_gl_error();
}

//======== Cube_map ========//

Cube_map::Cube_map()
{
    target_=GL_TEXTURE_CUBE_MAP;
}

void Cube_map::load(GLenum face, GLenum pixel_fmt, GLenum pixel_type, const void* data)
{
    bind();
    glTexSubImage2D(face, 0, 0, 0, width(), height(), pixel_fmt, pixel_type, data);
    check_gl_error();
}

//======== Texture_view ========//

Texture_view::Texture_view(GLenum target):
    Texture (target)
{

}

void Texture_view::link(const Texture& tx, GLenum frmt, GLuint lvl_offset, GLuint num_lvls, GLuint layer_offset_p, GLuint n_layers)
{
    raw_recreate();

    glTextureView(id(),target_,tx.id(),frmt,lvl_offset,num_lvls,layer_offset_p,n_layers);
    check_gl_error();

    levels_         = num_lvls;
    format_         = frmt;

    source_tx_      = &tx;
    lvl_offset_     = lvl_offset;
    layer_offset_   = layer_offset_p;
    num_layers_     = n_layers;

    restore_parameters();
}

} // namespace fgs
