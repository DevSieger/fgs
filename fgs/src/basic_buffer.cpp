/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/basic_buffer.hpp>
#include <fgs/shader.hpp>
#include <dk/logging.hpp>

namespace fgs {

//======== Buffer_storage ========//

Buffer_storage::Buffer_storage(GLenum a_usage):
    usage_(a_usage)
{
    glGenBuffers(1,&id_);
    check_gl_error();
}

std::shared_ptr<Buffer_storage> Buffer_storage::construct(GLenum a_usage)
{
    Buffer_storage* raw_ptr = new Buffer_storage(a_usage);
    return std::shared_ptr<Buffer_storage>(raw_ptr);
}

Buffer_storage::~Buffer_storage(){
    glDeleteBuffers(1,&id_);
}

void Buffer_storage::reallocate(size_t new_size){
    bind();
    allocated_=get_allocation_size(new_size);
    glBufferData(type(),allocated_, NULL, usage());
    check_gl_error();
}

//======== Buffer ========//


Buffer::Buffer(GLenum a_type, GLenum a_usage):
    type_(a_type), storage_( Buffer_storage::construct(a_usage))
{
}

Buffer::Buffer(GLenum a_type, Buffer_storage& storage):
    type_(a_type), storage_(storage.shared_from_this())
{
}

//======== Ibp_buffer ========//

Ibp_buffer::Ibp_buffer(GLenum a_type, GLuint a_binding_point, GLenum a_usage):
    Buffer(a_type, a_usage), binding_point_(a_binding_point)
{
}

Ibp_buffer::Ibp_buffer(GLenum a_type, GLuint a_binding_point, Buffer_storage& storage):
    Buffer(a_type, storage), binding_point_(a_binding_point)
{
}

void Ibp_buffer::bind_to_ibp(){
    if(id()){
        if(binding_size_>0){
            glBindBufferRange(type(),binding_point_,id(),binding_offset_,binding_size_);
            check_gl_error();
        }else{
            glBindBufferBase(type(),binding_point_,id());
            check_gl_error();
        }
    }
}

//======== UBO_base ========//

void UBO_base::set_binding(const Shader_program& shader, GLuint uniform_block_index){
    glUniformBlockBinding( shader.id(), uniform_block_index, binding_point() );
    check_gl_error();
}

void UBO_base::set_binding(const Shader_program& shader, const char* uniform_block_name){
    GLuint u_block_index = glGetUniformBlockIndex( shader.id(), uniform_block_name );
    if( u_block_index != GL_INVALID_INDEX ){
        set_binding(shader, u_block_index);
    }else{
        dk::log(dk::Detail_level::NON_CRITICAL,
                "not found uniform block %1% at shader program %2%, perhaps due to shader compiler optimization",
                uniform_block_name,shader.name());
    }
    check_gl_error();
}

//======== SSBO_base ========//

void SSBO_base::set_binding(const Shader_program& shader, const char* name){
    GLuint ix=glGetProgramResourceIndex(shader.id(),GL_SHADER_STORAGE_BLOCK,name);
    if(ix==GL_INVALID_INDEX){
        dk::log(dk::Detail_level::CRITICAL,"SSBO %1% not found in %2%",name,shader.name());
        return;
    }
    check_gl_error();
    glShaderStorageBlockBinding(shader.id(),ix,binding_point());
    check_gl_error();
}

} // namespace fgs
