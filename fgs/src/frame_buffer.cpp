/*
 * Copyright (c) 2019-2020, Nikita Parfenov
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#include <fgs/frame_buffer.hpp>
#include <fgs/texture.hpp>
#include <dk/logging.hpp>

namespace fgs {

Frame_buffer::Frame_buffer()
{
    init();
}

Frame_buffer::~Frame_buffer()
{
    if(id_){
        glDeleteFramebuffers(1,&id_);
    }
}

// to construct default fbo
Frame_buffer::Frame_buffer(GLuint id_):
    id_(id_)
{}

void Frame_buffer::init()
{
    glGenFramebuffers(1,&id_);
    check_gl_error();
}

void Frame_buffer::bind(GLenum mode)const
{
    glBindFramebuffer(mode,id_);
    check_gl_error();
}

bool Frame_buffer::check() const
{
    bind();
    GLenum status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
    if(status==GL_FRAMEBUFFER_COMPLETE){
        return true;
    }else{
        switch(status){
            case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_ATTACHMENT",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_MISSING_ATTACHMENT",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_DRAW_BUFFER",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_READ_BUFFER",id());
                break;
            case GL_FRAMEBUFFER_UNSUPPORTED:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: UNSUPPORTED",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_MULTISAMPLE",id());
                break;
            case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: INCOMPLETE_LAYER_TARGETS",id());
                break;
            case GL_FRAMEBUFFER_UNDEFINED:
                dk::log(dk::Detail_level::CRITICAL,"the FBO %1% status: UNDEFINED",id());
                break;
        };
        return false;
    };
}

bool Frame_buffer::attach_to_color(const Texture &tx, int attachment_index, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+attachment_index,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_to_depth(const Texture &tx, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_to_stencil(const Texture& tx, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_STENCIL_ATTACHMENT,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_to_depth_stencil(const Texture & tx, GLint level)
{
    bind();
    glFramebufferTexture(GL_DRAW_FRAMEBUFFER,GL_DEPTH_STENCIL_ATTACHMENT,tx.id(),level);
    return !check_gl_error();
}

bool Frame_buffer::attach_layer_to_color(const Texture &tx, int attachment_index, GLint layer, GLint level)
{
    bind();
    glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER,GL_COLOR_ATTACHMENT0+attachment_index,tx.id(),level,layer);
    return !check_gl_error();
}

bool Frame_buffer::attach_layer_to_depth(const Texture &tx, GLint layer, GLint level)
{
    bind();
    glFramebufferTextureLayer(GL_DRAW_FRAMEBUFFER,GL_DEPTH_ATTACHMENT,tx.id(),level,layer);
    return !check_gl_error();
}

Frame_buffer& Frame_buffer::default_fbo()
{
    static Frame_buffer s_default_fbo((GLuint)0);
    return s_default_fbo;
}

void Frame_buffer::map_attachments_raw(GLenum* indices, GLsizei count)
{
    GLint prev;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING,&prev);
    bind();
    check_gl_error();

    glDrawBuffers(count,indices);
    check_gl_error();

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER,prev);
    check_gl_error();
}

} // namespace fgs
