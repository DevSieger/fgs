#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include <fgs/gl_fun/gl_core_uq_4_3.hpp>
	
// to stop compiler whine about bad cast
namespace{
template<typename Lh_tp,typename Rh_tp>
bool set_loader_fptr(Lh_tp& lhv, Rh_tp rhv){
    lhv = reinterpret_cast<Lh_tp>(reinterpret_cast<void*>(rhv));
    return lhv;
}
} 


#if defined(__APPLE__)
#include <mach-o/dyld.h>

static void* AppleGLGetProcAddress (const GLubyte *name)
{
  static const struct mach_header* image = NULL;
  NSSymbol symbol;
  char* symbolName;
  if (NULL == image)
  {
    image = NSAddImage("/System/Library/Frameworks/OpenGL.framework/Versions/Current/OpenGL", NSADDIMAGE_OPTION_RETURN_ON_ERROR);
  }
  /* prepend a '_' for the Unix C symbol mangling convention */
  symbolName = malloc(strlen((const char*)name) + 2);
  strcpy(symbolName+1, (const char*)name);
  symbolName[0] = '_';
  symbol = NULL;
  /* if (NSIsSymbolNameDefined(symbolName))
	 symbol = NSLookupAndBindSymbol(symbolName); */
  symbol = image ? NSLookupSymbolInImage(image, symbolName, NSLOOKUPSYMBOLINIMAGE_OPTION_BIND | NSLOOKUPSYMBOLINIMAGE_OPTION_RETURN_ON_ERROR) : NULL;
  free(symbolName);
  return symbol ? NSAddressOfSymbol(symbol) : NULL;
}
#endif /* __APPLE__ */

#if defined(__sgi) || defined (__sun)
#include <dlfcn.h>
#include <stdio.h>

static void* SunGetProcAddress (const GLubyte* name)
{
  static void* h = NULL;
  static void* gpa;

  if (h == NULL)
  {
    if ((h = dlopen(NULL, RTLD_LAZY | RTLD_LOCAL)) == NULL) return NULL;
    gpa = dlsym(h, "glXGetProcAddress");
  }

  if (gpa != NULL)
    return ((void*(*)(const GLubyte*))gpa)(name);
  else
    return dlsym(h, (const char*)name);
}
#endif /* __sgi || __sun */

#if defined(_WIN32)

#ifdef _MSC_VER
#pragma warning(disable: 4055)
#pragma warning(disable: 4054)
#endif

static int TestPointer(const PROC pTest)
{
	ptrdiff_t iTest;
	if(!pTest) return 0;
    iTest = reinterpret_cast<ptrdiff_t>(pTest);
	
	if(iTest == 1 || iTest == 2 || iTest == 3 || iTest == -1) return 0;
	
	return 1;
}

static PROC WinGetProcAddress(const char *name)
{
    HMODULE glMod = nullptr;
    PROC pFunc = wglGetProcAddress(static_cast<LPCSTR>(name));
	if(TestPointer(pFunc))
	{
		return pFunc;
	}
	glMod = GetModuleHandleA("OpenGL32.dll");
    return static_cast<PROC>(GetProcAddress(glMod, static_cast<LPCSTR>(name)));
}
	
#define IntGetProcAddress(name) WinGetProcAddress(name)
#else
	#if defined(__APPLE__)
		#define IntGetProcAddress(name) AppleGLGetProcAddress(name)
	#else
		#if defined(__sgi) || defined(__sun)
			#define IntGetProcAddress(name) SunGetProcAddress(name)
		#else /* GLX */
		    #include <GL/glx.h>

			#define IntGetProcAddress(name) (*glXGetProcAddressARB)((const GLubyte*)name)
		#endif
	#endif
#endif

int ogl_ext_EXT_texture_compression_s3tc = ogl_LOAD_FAILED;
int ogl_ext_EXT_texture_sRGB = ogl_LOAD_FAILED;
int ogl_ext_EXT_texture_filter_anisotropic = ogl_LOAD_FAILED;

void (CODEGEN_FUNCPTR *_ptrc_glBlendFunc)(GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClear)(GLbitfield) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearColor)(GLfloat, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearDepth)(GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearStencil)(GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glColorMask)(GLboolean, GLboolean, GLboolean, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCullFace)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDepthFunc)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDepthMask)(GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDepthRange)(GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDisable)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawBuffer)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEnable)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFinish)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFlush)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFrontFace)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetBooleanv)(GLenum, GLboolean *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetDoublev)(GLenum, GLdouble *) = nullptr;
GLenum (CODEGEN_FUNCPTR *_ptrc_glGetError)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetFloatv)(GLenum, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetIntegerv)(GLenum, GLint *) = nullptr;
const GLubyte * (CODEGEN_FUNCPTR *_ptrc_glGetString)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexImage)(GLenum, GLint, GLenum, GLenum, GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexLevelParameterfv)(GLenum, GLint, GLenum, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexLevelParameteriv)(GLenum, GLint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexParameterfv)(GLenum, GLenum, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexParameteriv)(GLenum, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glHint)(GLenum, GLenum) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsEnabled)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glLineWidth)(GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glLogicOp)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPixelStoref)(GLenum, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPixelStorei)(GLenum, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPointSize)(GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPolygonMode)(GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glReadBuffer)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glReadPixels)(GLint, GLint, GLsizei, GLsizei, GLenum, GLenum, GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glScissor)(GLint, GLint, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glStencilFunc)(GLenum, GLint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glStencilMask)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glStencilOp)(GLenum, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexImage1D)(GLenum, GLint, GLint, GLsizei, GLint, GLenum, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexImage2D)(GLenum, GLint, GLint, GLsizei, GLsizei, GLint, GLenum, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexParameterf)(GLenum, GLenum, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexParameterfv)(GLenum, GLenum, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexParameteri)(GLenum, GLenum, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexParameteriv)(GLenum, GLenum, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glViewport)(GLint, GLint, GLsizei, GLsizei) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBindTexture)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCopyTexImage1D)(GLenum, GLint, GLenum, GLint, GLint, GLsizei, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCopyTexImage2D)(GLenum, GLint, GLenum, GLint, GLint, GLsizei, GLsizei, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCopyTexSubImage1D)(GLenum, GLint, GLint, GLint, GLint, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCopyTexSubImage2D)(GLenum, GLint, GLint, GLint, GLint, GLint, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteTextures)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawArrays)(GLenum, GLint, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElements)(GLenum, GLsizei, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenTextures)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetPointerv)(GLenum, GLvoid **) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsTexture)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPolygonOffset)(GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexSubImage1D)(GLenum, GLint, GLint, GLsizei, GLenum, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexSubImage2D)(GLenum, GLint, GLint, GLint, GLsizei, GLsizei, GLenum, GLenum, const GLvoid *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBlendColor)(GLfloat, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlendEquation)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCopyTexSubImage3D)(GLenum, GLint, GLint, GLint, GLint, GLint, GLint, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawRangeElements)(GLenum, GLuint, GLuint, GLsizei, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexImage3D)(GLenum, GLint, GLint, GLsizei, GLsizei, GLsizei, GLint, GLenum, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexSubImage3D)(GLenum, GLint, GLint, GLint, GLint, GLsizei, GLsizei, GLsizei, GLenum, GLenum, const GLvoid *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glActiveTexture)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompressedTexImage1D)(GLenum, GLint, GLenum, GLsizei, GLint, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompressedTexImage2D)(GLenum, GLint, GLenum, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompressedTexImage3D)(GLenum, GLint, GLenum, GLsizei, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompressedTexSubImage1D)(GLenum, GLint, GLint, GLsizei, GLenum, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompressedTexSubImage2D)(GLenum, GLint, GLint, GLint, GLsizei, GLsizei, GLenum, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompressedTexSubImage3D)(GLenum, GLint, GLint, GLint, GLint, GLsizei, GLsizei, GLsizei, GLenum, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetCompressedTexImage)(GLenum, GLint, GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSampleCoverage)(GLfloat, GLboolean) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBlendFuncSeparate)(GLenum, GLenum, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMultiDrawArrays)(GLenum, const GLint *, const GLsizei *, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMultiDrawElements)(GLenum, const GLsizei *, GLenum, const GLvoid *const*, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPointParameterf)(GLenum, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPointParameterfv)(GLenum, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPointParameteri)(GLenum, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPointParameteriv)(GLenum, const GLint *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBeginQuery)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindBuffer)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBufferData)(GLenum, GLsizeiptr, const GLvoid *, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBufferSubData)(GLenum, GLintptr, GLsizeiptr, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteBuffers)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteQueries)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEndQuery)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenBuffers)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenQueries)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetBufferParameteriv)(GLenum, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetBufferPointerv)(GLenum, GLenum, GLvoid **) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetBufferSubData)(GLenum, GLintptr, GLsizeiptr, GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetQueryObjectiv)(GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetQueryObjectuiv)(GLuint, GLenum, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetQueryiv)(GLenum, GLenum, GLint *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsBuffer)(GLuint) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsQuery)(GLuint) = nullptr;
void * (CODEGEN_FUNCPTR *_ptrc_glMapBuffer)(GLenum, GLenum) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glUnmapBuffer)(GLenum) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glAttachShader)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindAttribLocation)(GLuint, GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlendEquationSeparate)(GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCompileShader)(GLuint) = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glCreateProgram)() = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glCreateShader)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteProgram)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteShader)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDetachShader)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDisableVertexAttribArray)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawBuffers)(GLsizei, const GLenum *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEnableVertexAttribArray)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveAttrib)(GLuint, GLuint, GLsizei, GLsizei *, GLint *, GLenum *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveUniform)(GLuint, GLuint, GLsizei, GLsizei *, GLint *, GLenum *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetAttachedShaders)(GLuint, GLsizei, GLsizei *, GLuint *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetAttribLocation)(GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramInfoLog)(GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramiv)(GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetShaderInfoLog)(GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetShaderSource)(GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetShaderiv)(GLuint, GLenum, GLint *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetUniformLocation)(GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetUniformfv)(GLuint, GLint, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetUniformiv)(GLuint, GLint, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribPointerv)(GLuint, GLenum, GLvoid **) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribdv)(GLuint, GLenum, GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribfv)(GLuint, GLenum, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribiv)(GLuint, GLenum, GLint *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsProgram)(GLuint) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsShader)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glLinkProgram)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glShaderSource)(GLuint, GLsizei, const GLchar *const*, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glStencilFuncSeparate)(GLenum, GLenum, GLint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glStencilMaskSeparate)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glStencilOpSeparate)(GLenum, GLenum, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1f)(GLint, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1fv)(GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1i)(GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1iv)(GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2f)(GLint, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2fv)(GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2i)(GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2iv)(GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3f)(GLint, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3fv)(GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3i)(GLint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3iv)(GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4f)(GLint, GLfloat, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4fv)(GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4i)(GLint, GLint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4iv)(GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix2fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix3fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix4fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUseProgram)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glValidateProgram)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib1d)(GLuint, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib1dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib1f)(GLuint, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib1fv)(GLuint, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib1s)(GLuint, GLshort) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib1sv)(GLuint, const GLshort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib2d)(GLuint, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib2dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib2f)(GLuint, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib2fv)(GLuint, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib2s)(GLuint, GLshort, GLshort) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib2sv)(GLuint, const GLshort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib3d)(GLuint, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib3dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib3f)(GLuint, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib3fv)(GLuint, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib3s)(GLuint, GLshort, GLshort, GLshort) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib3sv)(GLuint, const GLshort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Nbv)(GLuint, const GLbyte *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Niv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Nsv)(GLuint, const GLshort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Nub)(GLuint, GLubyte, GLubyte, GLubyte, GLubyte) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Nubv)(GLuint, const GLubyte *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Nuiv)(GLuint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4Nusv)(GLuint, const GLushort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4bv)(GLuint, const GLbyte *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4d)(GLuint, GLdouble, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4f)(GLuint, GLfloat, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4fv)(GLuint, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4iv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4s)(GLuint, GLshort, GLshort, GLshort, GLshort) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4sv)(GLuint, const GLshort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4ubv)(GLuint, const GLubyte *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4uiv)(GLuint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttrib4usv)(GLuint, const GLushort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribPointer)(GLuint, GLint, GLenum, GLboolean, GLsizei, const GLvoid *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix2x3fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix2x4fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix3x2fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix3x4fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix4x2fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix4x3fv)(GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBeginConditionalRender)(GLuint, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBeginTransformFeedback)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindBufferBase)(GLenum, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindBufferRange)(GLenum, GLuint, GLuint, GLintptr, GLsizeiptr) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindFragDataLocation)(GLuint, GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindFramebuffer)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindRenderbuffer)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindVertexArray)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlitFramebuffer)(GLint, GLint, GLint, GLint, GLint, GLint, GLint, GLint, GLbitfield, GLenum) = nullptr;
GLenum (CODEGEN_FUNCPTR *_ptrc_glCheckFramebufferStatus)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClampColor)(GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearBufferfi)(GLenum, GLint, GLfloat, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearBufferfv)(GLenum, GLint, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearBufferiv)(GLenum, GLint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearBufferuiv)(GLenum, GLint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glColorMaski)(GLuint, GLboolean, GLboolean, GLboolean, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteFramebuffers)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteRenderbuffers)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteVertexArrays)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDisablei)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEnablei)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEndConditionalRender)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEndTransformFeedback)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFlushMappedBufferRange)(GLenum, GLintptr, GLsizeiptr) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferRenderbuffer)(GLenum, GLenum, GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferTexture1D)(GLenum, GLenum, GLenum, GLuint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferTexture2D)(GLenum, GLenum, GLenum, GLuint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferTexture3D)(GLenum, GLenum, GLenum, GLuint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferTextureLayer)(GLenum, GLenum, GLuint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenFramebuffers)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenRenderbuffers)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenVertexArrays)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenerateMipmap)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetBooleani_v)(GLenum, GLuint, GLboolean *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetFragDataLocation)(GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetFramebufferAttachmentParameteriv)(GLenum, GLenum, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetIntegeri_v)(GLenum, GLuint, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetRenderbufferParameteriv)(GLenum, GLenum, GLint *) = nullptr;
const GLubyte * (CODEGEN_FUNCPTR *_ptrc_glGetStringi)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexParameterIiv)(GLenum, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTexParameterIuiv)(GLenum, GLenum, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetTransformFeedbackVarying)(GLuint, GLuint, GLsizei, GLsizei *, GLsizei *, GLenum *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetUniformuiv)(GLuint, GLint, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribIiv)(GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribIuiv)(GLuint, GLenum, GLuint *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsEnabledi)(GLenum, GLuint) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsFramebuffer)(GLuint) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsRenderbuffer)(GLuint) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsVertexArray)(GLuint) = nullptr;
void * (CODEGEN_FUNCPTR *_ptrc_glMapBufferRange)(GLenum, GLintptr, GLsizeiptr, GLbitfield) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glRenderbufferStorage)(GLenum, GLenum, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glRenderbufferStorageMultisample)(GLenum, GLsizei, GLenum, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexParameterIiv)(GLenum, GLenum, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexParameterIuiv)(GLenum, GLenum, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTransformFeedbackVaryings)(GLuint, GLsizei, const GLchar *const*, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1ui)(GLint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1uiv)(GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2ui)(GLint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2uiv)(GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3ui)(GLint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3uiv)(GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4ui)(GLint, GLuint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4uiv)(GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI1i)(GLuint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI1iv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI1ui)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI1uiv)(GLuint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI2i)(GLuint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI2iv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI2ui)(GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI2uiv)(GLuint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI3i)(GLuint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI3iv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI3ui)(GLuint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI3uiv)(GLuint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4bv)(GLuint, const GLbyte *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4i)(GLuint, GLint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4iv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4sv)(GLuint, const GLshort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4ubv)(GLuint, const GLubyte *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4ui)(GLuint, GLuint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4uiv)(GLuint, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribI4usv)(GLuint, const GLushort *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribIPointer)(GLuint, GLint, GLenum, GLsizei, const GLvoid *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glCopyBufferSubData)(GLenum, GLenum, GLintptr, GLintptr, GLsizeiptr) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawArraysInstanced)(GLenum, GLint, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElementsInstanced)(GLenum, GLsizei, GLenum, const GLvoid *, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveUniformBlockName)(GLuint, GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveUniformBlockiv)(GLuint, GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveUniformName)(GLuint, GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveUniformsiv)(GLuint, GLsizei, const GLuint *, GLenum, GLint *) = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glGetUniformBlockIndex)(GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetUniformIndices)(GLuint, GLsizei, const GLchar *const*, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPrimitiveRestartIndex)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexBuffer)(GLenum, GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformBlockBinding)(GLuint, GLuint, GLuint) = nullptr;

GLenum (CODEGEN_FUNCPTR *_ptrc_glClientWaitSync)(GLsync, GLbitfield, GLuint64) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteSync)(GLsync) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElementsBaseVertex)(GLenum, GLsizei, GLenum, const GLvoid *, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElementsInstancedBaseVertex)(GLenum, GLsizei, GLenum, const GLvoid *, GLsizei, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawRangeElementsBaseVertex)(GLenum, GLuint, GLuint, GLsizei, GLenum, const GLvoid *, GLint) = nullptr;
GLsync (CODEGEN_FUNCPTR *_ptrc_glFenceSync)(GLenum, GLbitfield) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferTexture)(GLenum, GLenum, GLuint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetBufferParameteri64v)(GLenum, GLenum, GLint64 *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetInteger64i_v)(GLenum, GLuint, GLint64 *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetInteger64v)(GLenum, GLint64 *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetMultisamplefv)(GLenum, GLuint, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetSynciv)(GLsync, GLenum, GLsizei, GLsizei *, GLint *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsSync)(GLsync) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMultiDrawElementsBaseVertex)(GLenum, const GLsizei *, GLenum, const GLvoid *const*, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProvokingVertex)(GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSampleMaski)(GLuint, GLbitfield) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexImage2DMultisample)(GLenum, GLsizei, GLint, GLsizei, GLsizei, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexImage3DMultisample)(GLenum, GLsizei, GLint, GLsizei, GLsizei, GLsizei, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glWaitSync)(GLsync, GLbitfield, GLuint64) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBindFragDataLocationIndexed)(GLuint, GLuint, GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindSampler)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteSamplers)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenSamplers)(GLsizei, GLuint *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetFragDataIndex)(GLuint, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetQueryObjecti64v)(GLuint, GLenum, GLint64 *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetQueryObjectui64v)(GLuint, GLenum, GLuint64 *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetSamplerParameterIiv)(GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetSamplerParameterIuiv)(GLuint, GLenum, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetSamplerParameterfv)(GLuint, GLenum, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetSamplerParameteriv)(GLuint, GLenum, GLint *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsSampler)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glQueryCounter)(GLuint, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSamplerParameterIiv)(GLuint, GLenum, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSamplerParameterIuiv)(GLuint, GLenum, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSamplerParameterf)(GLuint, GLenum, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSamplerParameterfv)(GLuint, GLenum, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSamplerParameteri)(GLuint, GLenum, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glSamplerParameteriv)(GLuint, GLenum, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribDivisor)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP1ui)(GLuint, GLenum, GLboolean, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP1uiv)(GLuint, GLenum, GLboolean, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP2ui)(GLuint, GLenum, GLboolean, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP2uiv)(GLuint, GLenum, GLboolean, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP3ui)(GLuint, GLenum, GLboolean, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP3uiv)(GLuint, GLenum, GLboolean, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP4ui)(GLuint, GLenum, GLboolean, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribP4uiv)(GLuint, GLenum, GLboolean, const GLuint *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBeginQueryIndexed)(GLenum, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindTransformFeedback)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlendEquationSeparatei)(GLuint, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlendEquationi)(GLuint, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlendFuncSeparatei)(GLuint, GLenum, GLenum, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBlendFunci)(GLuint, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteTransformFeedbacks)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawArraysIndirect)(GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElementsIndirect)(GLenum, GLenum, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawTransformFeedback)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawTransformFeedbackStream)(GLenum, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glEndQueryIndexed)(GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenTransformFeedbacks)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveSubroutineName)(GLuint, GLenum, GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveSubroutineUniformName)(GLuint, GLenum, GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveSubroutineUniformiv)(GLuint, GLenum, GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramStageiv)(GLuint, GLenum, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetQueryIndexediv)(GLenum, GLuint, GLenum, GLint *) = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glGetSubroutineIndex)(GLuint, GLenum, const GLchar *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetSubroutineUniformLocation)(GLuint, GLenum, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetUniformSubroutineuiv)(GLenum, GLint, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetUniformdv)(GLuint, GLint, GLdouble *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsTransformFeedback)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMinSampleShading)(GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPatchParameterfv)(GLenum, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPatchParameteri)(GLenum, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPauseTransformFeedback)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glResumeTransformFeedback)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1d)(GLint, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform1dv)(GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2d)(GLint, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform2dv)(GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3d)(GLint, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform3dv)(GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4d)(GLint, GLdouble, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniform4dv)(GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix2dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix2x3dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix2x4dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix3dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix3x2dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix3x4dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix4dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix4x2dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformMatrix4x3dv)(GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUniformSubroutinesuiv)(GLenum, GLsizei, const GLuint *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glActiveShaderProgram)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glBindProgramPipeline)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearDepthf)(GLfloat) = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glCreateShaderProgramv)(GLenum, GLsizei, const GLchar *const*) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDeleteProgramPipelines)(GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDepthRangeArrayv)(GLuint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDepthRangeIndexed)(GLuint, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDepthRangef)(GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGenProgramPipelines)(GLsizei, GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetDoublei_v)(GLenum, GLuint, GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetFloati_v)(GLenum, GLuint, GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramBinary)(GLuint, GLsizei, GLsizei *, GLenum *, GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramPipelineInfoLog)(GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramPipelineiv)(GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetShaderPrecisionFormat)(GLenum, GLenum, GLint *, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetVertexAttribLdv)(GLuint, GLenum, GLdouble *) = nullptr;
GLboolean (CODEGEN_FUNCPTR *_ptrc_glIsProgramPipeline)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramBinary)(GLuint, GLenum, const GLvoid *, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramParameteri)(GLuint, GLenum, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1d)(GLuint, GLint, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1dv)(GLuint, GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1f)(GLuint, GLint, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1fv)(GLuint, GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1i)(GLuint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1iv)(GLuint, GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1ui)(GLuint, GLint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform1uiv)(GLuint, GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2d)(GLuint, GLint, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2dv)(GLuint, GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2f)(GLuint, GLint, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2fv)(GLuint, GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2i)(GLuint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2iv)(GLuint, GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2ui)(GLuint, GLint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform2uiv)(GLuint, GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3d)(GLuint, GLint, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3dv)(GLuint, GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3f)(GLuint, GLint, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3fv)(GLuint, GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3i)(GLuint, GLint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3iv)(GLuint, GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3ui)(GLuint, GLint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform3uiv)(GLuint, GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4d)(GLuint, GLint, GLdouble, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4dv)(GLuint, GLint, GLsizei, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4f)(GLuint, GLint, GLfloat, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4fv)(GLuint, GLint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4i)(GLuint, GLint, GLint, GLint, GLint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4iv)(GLuint, GLint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4ui)(GLuint, GLint, GLuint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniform4uiv)(GLuint, GLint, GLsizei, const GLuint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix2dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix2fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix2x3dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix2x3fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix2x4dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix2x4fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix3dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix3fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix3x2dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix3x2fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix3x4dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix3x4fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix4dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix4fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix4x2dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix4x2fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix4x3dv)(GLuint, GLint, GLsizei, GLboolean, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glProgramUniformMatrix4x3fv)(GLuint, GLint, GLsizei, GLboolean, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glReleaseShaderCompiler)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glScissorArrayv)(GLuint, GLsizei, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glScissorIndexed)(GLuint, GLint, GLint, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glScissorIndexedv)(GLuint, const GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glShaderBinary)(GLsizei, const GLuint *, GLenum, const GLvoid *, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glUseProgramStages)(GLuint, GLbitfield, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glValidateProgramPipeline)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL1d)(GLuint, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL1dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL2d)(GLuint, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL2dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL3d)(GLuint, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL3dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL4d)(GLuint, GLdouble, GLdouble, GLdouble, GLdouble) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribL4dv)(GLuint, const GLdouble *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribLPointer)(GLuint, GLint, GLenum, GLsizei, const GLvoid *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glViewportArrayv)(GLuint, GLsizei, const GLfloat *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glViewportIndexedf)(GLuint, GLfloat, GLfloat, GLfloat, GLfloat) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glViewportIndexedfv)(GLuint, const GLfloat *) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBindImageTexture)(GLuint, GLuint, GLint, GLboolean, GLint, GLenum, GLenum) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawArraysInstancedBaseInstance)(GLenum, GLint, GLsizei, GLsizei, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElementsInstancedBaseInstance)(GLenum, GLsizei, GLenum, const void *, GLsizei, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawElementsInstancedBaseVertexBaseInstance)(GLenum, GLsizei, GLenum, const void *, GLsizei, GLint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawTransformFeedbackInstanced)(GLenum, GLuint, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDrawTransformFeedbackStreamInstanced)(GLenum, GLuint, GLuint, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetActiveAtomicCounterBufferiv)(GLuint, GLuint, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetInternalformati64v)(GLenum, GLenum, GLenum, GLsizei, GLint64 *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMemoryBarrier)(GLbitfield) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexStorage1D)(GLenum, GLsizei, GLenum, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexStorage2D)(GLenum, GLsizei, GLenum, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexStorage3D)(GLenum, GLsizei, GLenum, GLsizei, GLsizei, GLsizei) = nullptr;

void (CODEGEN_FUNCPTR *_ptrc_glBindVertexBuffer)(GLuint, GLuint, GLintptr, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearBufferData)(GLenum, GLenum, GLenum, GLenum, const void *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glClearBufferSubData)(GLenum, GLenum, GLintptr, GLsizeiptr, GLenum, GLenum, const void *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glCopyImageSubData)(GLuint, GLenum, GLint, GLint, GLint, GLint, GLuint, GLenum, GLint, GLint, GLint, GLint, GLsizei, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDebugMessageCallback)(GLDEBUGPROC, const void *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDebugMessageControl)(GLenum, GLenum, GLenum, GLsizei, const GLuint *, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDebugMessageInsert)(GLenum, GLenum, GLuint, GLenum, GLsizei, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDispatchCompute)(GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glDispatchComputeIndirect)(GLintptr) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glFramebufferParameteri)(GLenum, GLenum, GLint) = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glGetDebugMessageLog)(GLuint, GLsizei, GLenum *, GLenum *, GLuint *, GLenum *, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetFramebufferParameteriv)(GLenum, GLenum, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetObjectLabel)(GLenum, GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetObjectPtrLabel)(const void *, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramInterfaceiv)(GLuint, GLenum, GLenum, GLint *) = nullptr;
GLuint (CODEGEN_FUNCPTR *_ptrc_glGetProgramResourceIndex)(GLuint, GLenum, const GLchar *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetProgramResourceLocation)(GLuint, GLenum, const GLchar *) = nullptr;
GLint (CODEGEN_FUNCPTR *_ptrc_glGetProgramResourceLocationIndex)(GLuint, GLenum, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramResourceName)(GLuint, GLenum, GLuint, GLsizei, GLsizei *, GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glGetProgramResourceiv)(GLuint, GLenum, GLuint, GLsizei, const GLenum *, GLsizei, GLsizei *, GLint *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glInvalidateBufferData)(GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glInvalidateBufferSubData)(GLuint, GLintptr, GLsizeiptr) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glInvalidateFramebuffer)(GLenum, GLsizei, const GLenum *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glInvalidateSubFramebuffer)(GLenum, GLsizei, const GLenum *, GLint, GLint, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glInvalidateTexImage)(GLuint, GLint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glInvalidateTexSubImage)(GLuint, GLint, GLint, GLint, GLint, GLsizei, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMultiDrawArraysIndirect)(GLenum, const void *, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glMultiDrawElementsIndirect)(GLenum, GLenum, const void *, GLsizei, GLsizei) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glObjectLabel)(GLenum, GLuint, GLsizei, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glObjectPtrLabel)(const void *, GLsizei, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPopDebugGroup)() = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glPushDebugGroup)(GLenum, GLuint, GLsizei, const GLchar *) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glShaderStorageBlockBinding)(GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexBufferRange)(GLenum, GLenum, GLuint, GLintptr, GLsizeiptr) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexStorage2DMultisample)(GLenum, GLsizei, GLenum, GLsizei, GLsizei, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTexStorage3DMultisample)(GLenum, GLsizei, GLenum, GLsizei, GLsizei, GLsizei, GLboolean) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glTextureView)(GLuint, GLenum, GLuint, GLenum, GLuint, GLuint, GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribBinding)(GLuint, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribFormat)(GLuint, GLint, GLenum, GLboolean, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribIFormat)(GLuint, GLint, GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexAttribLFormat)(GLuint, GLint, GLenum, GLuint) = nullptr;
void (CODEGEN_FUNCPTR *_ptrc_glVertexBindingDivisor)(GLuint, GLuint) = nullptr;

static int Load_Version_4_3()
{
	int numFailed = 0;
	if(!set_loader_fptr(_ptrc_glBlendFunc,IntGetProcAddress("glBlendFunc"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClear,IntGetProcAddress("glClear"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearColor,IntGetProcAddress("glClearColor"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearDepth,IntGetProcAddress("glClearDepth"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearStencil,IntGetProcAddress("glClearStencil"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glColorMask,IntGetProcAddress("glColorMask"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCullFace,IntGetProcAddress("glCullFace"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDepthFunc,IntGetProcAddress("glDepthFunc"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDepthMask,IntGetProcAddress("glDepthMask"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDepthRange,IntGetProcAddress("glDepthRange"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDisable,IntGetProcAddress("glDisable"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawBuffer,IntGetProcAddress("glDrawBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEnable,IntGetProcAddress("glEnable"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFinish,IntGetProcAddress("glFinish"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFlush,IntGetProcAddress("glFlush"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFrontFace,IntGetProcAddress("glFrontFace"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetBooleanv,IntGetProcAddress("glGetBooleanv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetDoublev,IntGetProcAddress("glGetDoublev"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetError,IntGetProcAddress("glGetError"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetFloatv,IntGetProcAddress("glGetFloatv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetIntegerv,IntGetProcAddress("glGetIntegerv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetString,IntGetProcAddress("glGetString"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexImage,IntGetProcAddress("glGetTexImage"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexLevelParameterfv,IntGetProcAddress("glGetTexLevelParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexLevelParameteriv,IntGetProcAddress("glGetTexLevelParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexParameterfv,IntGetProcAddress("glGetTexParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexParameteriv,IntGetProcAddress("glGetTexParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glHint,IntGetProcAddress("glHint"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsEnabled,IntGetProcAddress("glIsEnabled"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glLineWidth,IntGetProcAddress("glLineWidth"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glLogicOp,IntGetProcAddress("glLogicOp"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPixelStoref,IntGetProcAddress("glPixelStoref"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPixelStorei,IntGetProcAddress("glPixelStorei"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPointSize,IntGetProcAddress("glPointSize"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPolygonMode,IntGetProcAddress("glPolygonMode"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glReadBuffer,IntGetProcAddress("glReadBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glReadPixels,IntGetProcAddress("glReadPixels"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glScissor,IntGetProcAddress("glScissor"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glStencilFunc,IntGetProcAddress("glStencilFunc"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glStencilMask,IntGetProcAddress("glStencilMask"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glStencilOp,IntGetProcAddress("glStencilOp"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexImage1D,IntGetProcAddress("glTexImage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexImage2D,IntGetProcAddress("glTexImage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexParameterf,IntGetProcAddress("glTexParameterf"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexParameterfv,IntGetProcAddress("glTexParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexParameteri,IntGetProcAddress("glTexParameteri"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexParameteriv,IntGetProcAddress("glTexParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glViewport,IntGetProcAddress("glViewport"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindTexture,IntGetProcAddress("glBindTexture"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyTexImage1D,IntGetProcAddress("glCopyTexImage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyTexImage2D,IntGetProcAddress("glCopyTexImage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyTexSubImage1D,IntGetProcAddress("glCopyTexSubImage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyTexSubImage2D,IntGetProcAddress("glCopyTexSubImage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteTextures,IntGetProcAddress("glDeleteTextures"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawArrays,IntGetProcAddress("glDrawArrays"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElements,IntGetProcAddress("glDrawElements"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenTextures,IntGetProcAddress("glGenTextures"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetPointerv,IntGetProcAddress("glGetPointerv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsTexture,IntGetProcAddress("glIsTexture"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPolygonOffset,IntGetProcAddress("glPolygonOffset"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexSubImage1D,IntGetProcAddress("glTexSubImage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexSubImage2D,IntGetProcAddress("glTexSubImage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendColor,IntGetProcAddress("glBlendColor"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendEquation,IntGetProcAddress("glBlendEquation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyTexSubImage3D,IntGetProcAddress("glCopyTexSubImage3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawRangeElements,IntGetProcAddress("glDrawRangeElements"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexImage3D,IntGetProcAddress("glTexImage3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexSubImage3D,IntGetProcAddress("glTexSubImage3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glActiveTexture,IntGetProcAddress("glActiveTexture"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompressedTexImage1D,IntGetProcAddress("glCompressedTexImage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompressedTexImage2D,IntGetProcAddress("glCompressedTexImage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompressedTexImage3D,IntGetProcAddress("glCompressedTexImage3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompressedTexSubImage1D,IntGetProcAddress("glCompressedTexSubImage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompressedTexSubImage2D,IntGetProcAddress("glCompressedTexSubImage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompressedTexSubImage3D,IntGetProcAddress("glCompressedTexSubImage3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetCompressedTexImage,IntGetProcAddress("glGetCompressedTexImage"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSampleCoverage,IntGetProcAddress("glSampleCoverage"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendFuncSeparate,IntGetProcAddress("glBlendFuncSeparate"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMultiDrawArrays,IntGetProcAddress("glMultiDrawArrays"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMultiDrawElements,IntGetProcAddress("glMultiDrawElements"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPointParameterf,IntGetProcAddress("glPointParameterf"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPointParameterfv,IntGetProcAddress("glPointParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPointParameteri,IntGetProcAddress("glPointParameteri"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPointParameteriv,IntGetProcAddress("glPointParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBeginQuery,IntGetProcAddress("glBeginQuery"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindBuffer,IntGetProcAddress("glBindBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBufferData,IntGetProcAddress("glBufferData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBufferSubData,IntGetProcAddress("glBufferSubData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteBuffers,IntGetProcAddress("glDeleteBuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteQueries,IntGetProcAddress("glDeleteQueries"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEndQuery,IntGetProcAddress("glEndQuery"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenBuffers,IntGetProcAddress("glGenBuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenQueries,IntGetProcAddress("glGenQueries"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetBufferParameteriv,IntGetProcAddress("glGetBufferParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetBufferPointerv,IntGetProcAddress("glGetBufferPointerv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetBufferSubData,IntGetProcAddress("glGetBufferSubData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetQueryObjectiv,IntGetProcAddress("glGetQueryObjectiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetQueryObjectuiv,IntGetProcAddress("glGetQueryObjectuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetQueryiv,IntGetProcAddress("glGetQueryiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsBuffer,IntGetProcAddress("glIsBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsQuery,IntGetProcAddress("glIsQuery"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMapBuffer,IntGetProcAddress("glMapBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUnmapBuffer,IntGetProcAddress("glUnmapBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glAttachShader,IntGetProcAddress("glAttachShader"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindAttribLocation,IntGetProcAddress("glBindAttribLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendEquationSeparate,IntGetProcAddress("glBlendEquationSeparate"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCompileShader,IntGetProcAddress("glCompileShader"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCreateProgram,IntGetProcAddress("glCreateProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCreateShader,IntGetProcAddress("glCreateShader"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteProgram,IntGetProcAddress("glDeleteProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteShader,IntGetProcAddress("glDeleteShader"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDetachShader,IntGetProcAddress("glDetachShader"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDisableVertexAttribArray,IntGetProcAddress("glDisableVertexAttribArray"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawBuffers,IntGetProcAddress("glDrawBuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEnableVertexAttribArray,IntGetProcAddress("glEnableVertexAttribArray"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveAttrib,IntGetProcAddress("glGetActiveAttrib"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveUniform,IntGetProcAddress("glGetActiveUniform"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetAttachedShaders,IntGetProcAddress("glGetAttachedShaders"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetAttribLocation,IntGetProcAddress("glGetAttribLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramInfoLog,IntGetProcAddress("glGetProgramInfoLog"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramiv,IntGetProcAddress("glGetProgramiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetShaderInfoLog,IntGetProcAddress("glGetShaderInfoLog"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetShaderSource,IntGetProcAddress("glGetShaderSource"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetShaderiv,IntGetProcAddress("glGetShaderiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformLocation,IntGetProcAddress("glGetUniformLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformfv,IntGetProcAddress("glGetUniformfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformiv,IntGetProcAddress("glGetUniformiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribPointerv,IntGetProcAddress("glGetVertexAttribPointerv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribdv,IntGetProcAddress("glGetVertexAttribdv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribfv,IntGetProcAddress("glGetVertexAttribfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribiv,IntGetProcAddress("glGetVertexAttribiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsProgram,IntGetProcAddress("glIsProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsShader,IntGetProcAddress("glIsShader"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glLinkProgram,IntGetProcAddress("glLinkProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glShaderSource,IntGetProcAddress("glShaderSource"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glStencilFuncSeparate,IntGetProcAddress("glStencilFuncSeparate"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glStencilMaskSeparate,IntGetProcAddress("glStencilMaskSeparate"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glStencilOpSeparate,IntGetProcAddress("glStencilOpSeparate"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1f,IntGetProcAddress("glUniform1f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1fv,IntGetProcAddress("glUniform1fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1i,IntGetProcAddress("glUniform1i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1iv,IntGetProcAddress("glUniform1iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2f,IntGetProcAddress("glUniform2f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2fv,IntGetProcAddress("glUniform2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2i,IntGetProcAddress("glUniform2i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2iv,IntGetProcAddress("glUniform2iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3f,IntGetProcAddress("glUniform3f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3fv,IntGetProcAddress("glUniform3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3i,IntGetProcAddress("glUniform3i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3iv,IntGetProcAddress("glUniform3iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4f,IntGetProcAddress("glUniform4f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4fv,IntGetProcAddress("glUniform4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4i,IntGetProcAddress("glUniform4i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4iv,IntGetProcAddress("glUniform4iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix2fv,IntGetProcAddress("glUniformMatrix2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix3fv,IntGetProcAddress("glUniformMatrix3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix4fv,IntGetProcAddress("glUniformMatrix4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUseProgram,IntGetProcAddress("glUseProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glValidateProgram,IntGetProcAddress("glValidateProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib1d,IntGetProcAddress("glVertexAttrib1d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib1dv,IntGetProcAddress("glVertexAttrib1dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib1f,IntGetProcAddress("glVertexAttrib1f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib1fv,IntGetProcAddress("glVertexAttrib1fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib1s,IntGetProcAddress("glVertexAttrib1s"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib1sv,IntGetProcAddress("glVertexAttrib1sv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib2d,IntGetProcAddress("glVertexAttrib2d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib2dv,IntGetProcAddress("glVertexAttrib2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib2f,IntGetProcAddress("glVertexAttrib2f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib2fv,IntGetProcAddress("glVertexAttrib2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib2s,IntGetProcAddress("glVertexAttrib2s"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib2sv,IntGetProcAddress("glVertexAttrib2sv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib3d,IntGetProcAddress("glVertexAttrib3d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib3dv,IntGetProcAddress("glVertexAttrib3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib3f,IntGetProcAddress("glVertexAttrib3f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib3fv,IntGetProcAddress("glVertexAttrib3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib3s,IntGetProcAddress("glVertexAttrib3s"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib3sv,IntGetProcAddress("glVertexAttrib3sv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Nbv,IntGetProcAddress("glVertexAttrib4Nbv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Niv,IntGetProcAddress("glVertexAttrib4Niv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Nsv,IntGetProcAddress("glVertexAttrib4Nsv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Nub,IntGetProcAddress("glVertexAttrib4Nub"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Nubv,IntGetProcAddress("glVertexAttrib4Nubv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Nuiv,IntGetProcAddress("glVertexAttrib4Nuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4Nusv,IntGetProcAddress("glVertexAttrib4Nusv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4bv,IntGetProcAddress("glVertexAttrib4bv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4d,IntGetProcAddress("glVertexAttrib4d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4dv,IntGetProcAddress("glVertexAttrib4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4f,IntGetProcAddress("glVertexAttrib4f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4fv,IntGetProcAddress("glVertexAttrib4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4iv,IntGetProcAddress("glVertexAttrib4iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4s,IntGetProcAddress("glVertexAttrib4s"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4sv,IntGetProcAddress("glVertexAttrib4sv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4ubv,IntGetProcAddress("glVertexAttrib4ubv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4uiv,IntGetProcAddress("glVertexAttrib4uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttrib4usv,IntGetProcAddress("glVertexAttrib4usv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribPointer,IntGetProcAddress("glVertexAttribPointer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix2x3fv,IntGetProcAddress("glUniformMatrix2x3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix2x4fv,IntGetProcAddress("glUniformMatrix2x4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix3x2fv,IntGetProcAddress("glUniformMatrix3x2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix3x4fv,IntGetProcAddress("glUniformMatrix3x4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix4x2fv,IntGetProcAddress("glUniformMatrix4x2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix4x3fv,IntGetProcAddress("glUniformMatrix4x3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBeginConditionalRender,IntGetProcAddress("glBeginConditionalRender"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBeginTransformFeedback,IntGetProcAddress("glBeginTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindBufferBase,IntGetProcAddress("glBindBufferBase"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindBufferRange,IntGetProcAddress("glBindBufferRange"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindFragDataLocation,IntGetProcAddress("glBindFragDataLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindFramebuffer,IntGetProcAddress("glBindFramebuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindRenderbuffer,IntGetProcAddress("glBindRenderbuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindVertexArray,IntGetProcAddress("glBindVertexArray"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlitFramebuffer,IntGetProcAddress("glBlitFramebuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCheckFramebufferStatus,IntGetProcAddress("glCheckFramebufferStatus"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClampColor,IntGetProcAddress("glClampColor"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearBufferfi,IntGetProcAddress("glClearBufferfi"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearBufferfv,IntGetProcAddress("glClearBufferfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearBufferiv,IntGetProcAddress("glClearBufferiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearBufferuiv,IntGetProcAddress("glClearBufferuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glColorMaski,IntGetProcAddress("glColorMaski"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteFramebuffers,IntGetProcAddress("glDeleteFramebuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteRenderbuffers,IntGetProcAddress("glDeleteRenderbuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteVertexArrays,IntGetProcAddress("glDeleteVertexArrays"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDisablei,IntGetProcAddress("glDisablei"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEnablei,IntGetProcAddress("glEnablei"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEndConditionalRender,IntGetProcAddress("glEndConditionalRender"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEndTransformFeedback,IntGetProcAddress("glEndTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFlushMappedBufferRange,IntGetProcAddress("glFlushMappedBufferRange"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferRenderbuffer,IntGetProcAddress("glFramebufferRenderbuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferTexture1D,IntGetProcAddress("glFramebufferTexture1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferTexture2D,IntGetProcAddress("glFramebufferTexture2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferTexture3D,IntGetProcAddress("glFramebufferTexture3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferTextureLayer,IntGetProcAddress("glFramebufferTextureLayer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenFramebuffers,IntGetProcAddress("glGenFramebuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenRenderbuffers,IntGetProcAddress("glGenRenderbuffers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenVertexArrays,IntGetProcAddress("glGenVertexArrays"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenerateMipmap,IntGetProcAddress("glGenerateMipmap"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetBooleani_v,IntGetProcAddress("glGetBooleani_v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetFragDataLocation,IntGetProcAddress("glGetFragDataLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetFramebufferAttachmentParameteriv,IntGetProcAddress("glGetFramebufferAttachmentParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetIntegeri_v,IntGetProcAddress("glGetIntegeri_v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetRenderbufferParameteriv,IntGetProcAddress("glGetRenderbufferParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetStringi,IntGetProcAddress("glGetStringi"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexParameterIiv,IntGetProcAddress("glGetTexParameterIiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTexParameterIuiv,IntGetProcAddress("glGetTexParameterIuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetTransformFeedbackVarying,IntGetProcAddress("glGetTransformFeedbackVarying"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformuiv,IntGetProcAddress("glGetUniformuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribIiv,IntGetProcAddress("glGetVertexAttribIiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribIuiv,IntGetProcAddress("glGetVertexAttribIuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsEnabledi,IntGetProcAddress("glIsEnabledi"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsFramebuffer,IntGetProcAddress("glIsFramebuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsRenderbuffer,IntGetProcAddress("glIsRenderbuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsVertexArray,IntGetProcAddress("glIsVertexArray"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMapBufferRange,IntGetProcAddress("glMapBufferRange"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glRenderbufferStorage,IntGetProcAddress("glRenderbufferStorage"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glRenderbufferStorageMultisample,IntGetProcAddress("glRenderbufferStorageMultisample"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexParameterIiv,IntGetProcAddress("glTexParameterIiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexParameterIuiv,IntGetProcAddress("glTexParameterIuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTransformFeedbackVaryings,IntGetProcAddress("glTransformFeedbackVaryings"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1ui,IntGetProcAddress("glUniform1ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1uiv,IntGetProcAddress("glUniform1uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2ui,IntGetProcAddress("glUniform2ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2uiv,IntGetProcAddress("glUniform2uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3ui,IntGetProcAddress("glUniform3ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3uiv,IntGetProcAddress("glUniform3uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4ui,IntGetProcAddress("glUniform4ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4uiv,IntGetProcAddress("glUniform4uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI1i,IntGetProcAddress("glVertexAttribI1i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI1iv,IntGetProcAddress("glVertexAttribI1iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI1ui,IntGetProcAddress("glVertexAttribI1ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI1uiv,IntGetProcAddress("glVertexAttribI1uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI2i,IntGetProcAddress("glVertexAttribI2i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI2iv,IntGetProcAddress("glVertexAttribI2iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI2ui,IntGetProcAddress("glVertexAttribI2ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI2uiv,IntGetProcAddress("glVertexAttribI2uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI3i,IntGetProcAddress("glVertexAttribI3i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI3iv,IntGetProcAddress("glVertexAttribI3iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI3ui,IntGetProcAddress("glVertexAttribI3ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI3uiv,IntGetProcAddress("glVertexAttribI3uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4bv,IntGetProcAddress("glVertexAttribI4bv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4i,IntGetProcAddress("glVertexAttribI4i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4iv,IntGetProcAddress("glVertexAttribI4iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4sv,IntGetProcAddress("glVertexAttribI4sv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4ubv,IntGetProcAddress("glVertexAttribI4ubv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4ui,IntGetProcAddress("glVertexAttribI4ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4uiv,IntGetProcAddress("glVertexAttribI4uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribI4usv,IntGetProcAddress("glVertexAttribI4usv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribIPointer,IntGetProcAddress("glVertexAttribIPointer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyBufferSubData,IntGetProcAddress("glCopyBufferSubData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawArraysInstanced,IntGetProcAddress("glDrawArraysInstanced"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElementsInstanced,IntGetProcAddress("glDrawElementsInstanced"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveUniformBlockName,IntGetProcAddress("glGetActiveUniformBlockName"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveUniformBlockiv,IntGetProcAddress("glGetActiveUniformBlockiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveUniformName,IntGetProcAddress("glGetActiveUniformName"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveUniformsiv,IntGetProcAddress("glGetActiveUniformsiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformBlockIndex,IntGetProcAddress("glGetUniformBlockIndex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformIndices,IntGetProcAddress("glGetUniformIndices"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPrimitiveRestartIndex,IntGetProcAddress("glPrimitiveRestartIndex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexBuffer,IntGetProcAddress("glTexBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformBlockBinding,IntGetProcAddress("glUniformBlockBinding"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClientWaitSync,IntGetProcAddress("glClientWaitSync"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteSync,IntGetProcAddress("glDeleteSync"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElementsBaseVertex,IntGetProcAddress("glDrawElementsBaseVertex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElementsInstancedBaseVertex,IntGetProcAddress("glDrawElementsInstancedBaseVertex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawRangeElementsBaseVertex,IntGetProcAddress("glDrawRangeElementsBaseVertex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFenceSync,IntGetProcAddress("glFenceSync"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferTexture,IntGetProcAddress("glFramebufferTexture"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetBufferParameteri64v,IntGetProcAddress("glGetBufferParameteri64v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetInteger64i_v,IntGetProcAddress("glGetInteger64i_v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetInteger64v,IntGetProcAddress("glGetInteger64v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetMultisamplefv,IntGetProcAddress("glGetMultisamplefv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSynciv,IntGetProcAddress("glGetSynciv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsSync,IntGetProcAddress("glIsSync"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMultiDrawElementsBaseVertex,IntGetProcAddress("glMultiDrawElementsBaseVertex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProvokingVertex,IntGetProcAddress("glProvokingVertex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSampleMaski,IntGetProcAddress("glSampleMaski"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexImage2DMultisample,IntGetProcAddress("glTexImage2DMultisample"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexImage3DMultisample,IntGetProcAddress("glTexImage3DMultisample"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glWaitSync,IntGetProcAddress("glWaitSync"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindFragDataLocationIndexed,IntGetProcAddress("glBindFragDataLocationIndexed"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindSampler,IntGetProcAddress("glBindSampler"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteSamplers,IntGetProcAddress("glDeleteSamplers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenSamplers,IntGetProcAddress("glGenSamplers"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetFragDataIndex,IntGetProcAddress("glGetFragDataIndex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetQueryObjecti64v,IntGetProcAddress("glGetQueryObjecti64v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetQueryObjectui64v,IntGetProcAddress("glGetQueryObjectui64v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSamplerParameterIiv,IntGetProcAddress("glGetSamplerParameterIiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSamplerParameterIuiv,IntGetProcAddress("glGetSamplerParameterIuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSamplerParameterfv,IntGetProcAddress("glGetSamplerParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSamplerParameteriv,IntGetProcAddress("glGetSamplerParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsSampler,IntGetProcAddress("glIsSampler"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glQueryCounter,IntGetProcAddress("glQueryCounter"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSamplerParameterIiv,IntGetProcAddress("glSamplerParameterIiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSamplerParameterIuiv,IntGetProcAddress("glSamplerParameterIuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSamplerParameterf,IntGetProcAddress("glSamplerParameterf"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSamplerParameterfv,IntGetProcAddress("glSamplerParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSamplerParameteri,IntGetProcAddress("glSamplerParameteri"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glSamplerParameteriv,IntGetProcAddress("glSamplerParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribDivisor,IntGetProcAddress("glVertexAttribDivisor"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP1ui,IntGetProcAddress("glVertexAttribP1ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP1uiv,IntGetProcAddress("glVertexAttribP1uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP2ui,IntGetProcAddress("glVertexAttribP2ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP2uiv,IntGetProcAddress("glVertexAttribP2uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP3ui,IntGetProcAddress("glVertexAttribP3ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP3uiv,IntGetProcAddress("glVertexAttribP3uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP4ui,IntGetProcAddress("glVertexAttribP4ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribP4uiv,IntGetProcAddress("glVertexAttribP4uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBeginQueryIndexed,IntGetProcAddress("glBeginQueryIndexed"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindTransformFeedback,IntGetProcAddress("glBindTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendEquationSeparatei,IntGetProcAddress("glBlendEquationSeparatei"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendEquationi,IntGetProcAddress("glBlendEquationi"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendFuncSeparatei,IntGetProcAddress("glBlendFuncSeparatei"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBlendFunci,IntGetProcAddress("glBlendFunci"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteTransformFeedbacks,IntGetProcAddress("glDeleteTransformFeedbacks"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawArraysIndirect,IntGetProcAddress("glDrawArraysIndirect"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElementsIndirect,IntGetProcAddress("glDrawElementsIndirect"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawTransformFeedback,IntGetProcAddress("glDrawTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawTransformFeedbackStream,IntGetProcAddress("glDrawTransformFeedbackStream"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glEndQueryIndexed,IntGetProcAddress("glEndQueryIndexed"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenTransformFeedbacks,IntGetProcAddress("glGenTransformFeedbacks"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveSubroutineName,IntGetProcAddress("glGetActiveSubroutineName"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveSubroutineUniformName,IntGetProcAddress("glGetActiveSubroutineUniformName"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveSubroutineUniformiv,IntGetProcAddress("glGetActiveSubroutineUniformiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramStageiv,IntGetProcAddress("glGetProgramStageiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetQueryIndexediv,IntGetProcAddress("glGetQueryIndexediv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSubroutineIndex,IntGetProcAddress("glGetSubroutineIndex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetSubroutineUniformLocation,IntGetProcAddress("glGetSubroutineUniformLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformSubroutineuiv,IntGetProcAddress("glGetUniformSubroutineuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetUniformdv,IntGetProcAddress("glGetUniformdv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsTransformFeedback,IntGetProcAddress("glIsTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMinSampleShading,IntGetProcAddress("glMinSampleShading"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPatchParameterfv,IntGetProcAddress("glPatchParameterfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPatchParameteri,IntGetProcAddress("glPatchParameteri"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPauseTransformFeedback,IntGetProcAddress("glPauseTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glResumeTransformFeedback,IntGetProcAddress("glResumeTransformFeedback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1d,IntGetProcAddress("glUniform1d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform1dv,IntGetProcAddress("glUniform1dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2d,IntGetProcAddress("glUniform2d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform2dv,IntGetProcAddress("glUniform2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3d,IntGetProcAddress("glUniform3d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform3dv,IntGetProcAddress("glUniform3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4d,IntGetProcAddress("glUniform4d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniform4dv,IntGetProcAddress("glUniform4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix2dv,IntGetProcAddress("glUniformMatrix2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix2x3dv,IntGetProcAddress("glUniformMatrix2x3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix2x4dv,IntGetProcAddress("glUniformMatrix2x4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix3dv,IntGetProcAddress("glUniformMatrix3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix3x2dv,IntGetProcAddress("glUniformMatrix3x2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix3x4dv,IntGetProcAddress("glUniformMatrix3x4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix4dv,IntGetProcAddress("glUniformMatrix4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix4x2dv,IntGetProcAddress("glUniformMatrix4x2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformMatrix4x3dv,IntGetProcAddress("glUniformMatrix4x3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUniformSubroutinesuiv,IntGetProcAddress("glUniformSubroutinesuiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glActiveShaderProgram,IntGetProcAddress("glActiveShaderProgram"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindProgramPipeline,IntGetProcAddress("glBindProgramPipeline"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearDepthf,IntGetProcAddress("glClearDepthf"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCreateShaderProgramv,IntGetProcAddress("glCreateShaderProgramv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDeleteProgramPipelines,IntGetProcAddress("glDeleteProgramPipelines"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDepthRangeArrayv,IntGetProcAddress("glDepthRangeArrayv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDepthRangeIndexed,IntGetProcAddress("glDepthRangeIndexed"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDepthRangef,IntGetProcAddress("glDepthRangef"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGenProgramPipelines,IntGetProcAddress("glGenProgramPipelines"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetDoublei_v,IntGetProcAddress("glGetDoublei_v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetFloati_v,IntGetProcAddress("glGetFloati_v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramBinary,IntGetProcAddress("glGetProgramBinary"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramPipelineInfoLog,IntGetProcAddress("glGetProgramPipelineInfoLog"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramPipelineiv,IntGetProcAddress("glGetProgramPipelineiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetShaderPrecisionFormat,IntGetProcAddress("glGetShaderPrecisionFormat"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetVertexAttribLdv,IntGetProcAddress("glGetVertexAttribLdv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glIsProgramPipeline,IntGetProcAddress("glIsProgramPipeline"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramBinary,IntGetProcAddress("glProgramBinary"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramParameteri,IntGetProcAddress("glProgramParameteri"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1d,IntGetProcAddress("glProgramUniform1d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1dv,IntGetProcAddress("glProgramUniform1dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1f,IntGetProcAddress("glProgramUniform1f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1fv,IntGetProcAddress("glProgramUniform1fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1i,IntGetProcAddress("glProgramUniform1i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1iv,IntGetProcAddress("glProgramUniform1iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1ui,IntGetProcAddress("glProgramUniform1ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform1uiv,IntGetProcAddress("glProgramUniform1uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2d,IntGetProcAddress("glProgramUniform2d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2dv,IntGetProcAddress("glProgramUniform2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2f,IntGetProcAddress("glProgramUniform2f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2fv,IntGetProcAddress("glProgramUniform2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2i,IntGetProcAddress("glProgramUniform2i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2iv,IntGetProcAddress("glProgramUniform2iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2ui,IntGetProcAddress("glProgramUniform2ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform2uiv,IntGetProcAddress("glProgramUniform2uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3d,IntGetProcAddress("glProgramUniform3d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3dv,IntGetProcAddress("glProgramUniform3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3f,IntGetProcAddress("glProgramUniform3f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3fv,IntGetProcAddress("glProgramUniform3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3i,IntGetProcAddress("glProgramUniform3i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3iv,IntGetProcAddress("glProgramUniform3iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3ui,IntGetProcAddress("glProgramUniform3ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform3uiv,IntGetProcAddress("glProgramUniform3uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4d,IntGetProcAddress("glProgramUniform4d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4dv,IntGetProcAddress("glProgramUniform4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4f,IntGetProcAddress("glProgramUniform4f"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4fv,IntGetProcAddress("glProgramUniform4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4i,IntGetProcAddress("glProgramUniform4i"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4iv,IntGetProcAddress("glProgramUniform4iv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4ui,IntGetProcAddress("glProgramUniform4ui"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniform4uiv,IntGetProcAddress("glProgramUniform4uiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix2dv,IntGetProcAddress("glProgramUniformMatrix2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix2fv,IntGetProcAddress("glProgramUniformMatrix2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix2x3dv,IntGetProcAddress("glProgramUniformMatrix2x3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix2x3fv,IntGetProcAddress("glProgramUniformMatrix2x3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix2x4dv,IntGetProcAddress("glProgramUniformMatrix2x4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix2x4fv,IntGetProcAddress("glProgramUniformMatrix2x4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix3dv,IntGetProcAddress("glProgramUniformMatrix3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix3fv,IntGetProcAddress("glProgramUniformMatrix3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix3x2dv,IntGetProcAddress("glProgramUniformMatrix3x2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix3x2fv,IntGetProcAddress("glProgramUniformMatrix3x2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix3x4dv,IntGetProcAddress("glProgramUniformMatrix3x4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix3x4fv,IntGetProcAddress("glProgramUniformMatrix3x4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix4dv,IntGetProcAddress("glProgramUniformMatrix4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix4fv,IntGetProcAddress("glProgramUniformMatrix4fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix4x2dv,IntGetProcAddress("glProgramUniformMatrix4x2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix4x2fv,IntGetProcAddress("glProgramUniformMatrix4x2fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix4x3dv,IntGetProcAddress("glProgramUniformMatrix4x3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glProgramUniformMatrix4x3fv,IntGetProcAddress("glProgramUniformMatrix4x3fv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glReleaseShaderCompiler,IntGetProcAddress("glReleaseShaderCompiler"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glScissorArrayv,IntGetProcAddress("glScissorArrayv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glScissorIndexed,IntGetProcAddress("glScissorIndexed"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glScissorIndexedv,IntGetProcAddress("glScissorIndexedv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glShaderBinary,IntGetProcAddress("glShaderBinary"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glUseProgramStages,IntGetProcAddress("glUseProgramStages"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glValidateProgramPipeline,IntGetProcAddress("glValidateProgramPipeline"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL1d,IntGetProcAddress("glVertexAttribL1d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL1dv,IntGetProcAddress("glVertexAttribL1dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL2d,IntGetProcAddress("glVertexAttribL2d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL2dv,IntGetProcAddress("glVertexAttribL2dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL3d,IntGetProcAddress("glVertexAttribL3d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL3dv,IntGetProcAddress("glVertexAttribL3dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL4d,IntGetProcAddress("glVertexAttribL4d"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribL4dv,IntGetProcAddress("glVertexAttribL4dv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribLPointer,IntGetProcAddress("glVertexAttribLPointer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glViewportArrayv,IntGetProcAddress("glViewportArrayv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glViewportIndexedf,IntGetProcAddress("glViewportIndexedf"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glViewportIndexedfv,IntGetProcAddress("glViewportIndexedfv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindImageTexture,IntGetProcAddress("glBindImageTexture"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawArraysInstancedBaseInstance,IntGetProcAddress("glDrawArraysInstancedBaseInstance"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElementsInstancedBaseInstance,IntGetProcAddress("glDrawElementsInstancedBaseInstance"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawElementsInstancedBaseVertexBaseInstance,IntGetProcAddress("glDrawElementsInstancedBaseVertexBaseInstance"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawTransformFeedbackInstanced,IntGetProcAddress("glDrawTransformFeedbackInstanced"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDrawTransformFeedbackStreamInstanced,IntGetProcAddress("glDrawTransformFeedbackStreamInstanced"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetActiveAtomicCounterBufferiv,IntGetProcAddress("glGetActiveAtomicCounterBufferiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetInternalformati64v,IntGetProcAddress("glGetInternalformati64v"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMemoryBarrier,IntGetProcAddress("glMemoryBarrier"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexStorage1D,IntGetProcAddress("glTexStorage1D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexStorage2D,IntGetProcAddress("glTexStorage2D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexStorage3D,IntGetProcAddress("glTexStorage3D"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glBindVertexBuffer,IntGetProcAddress("glBindVertexBuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearBufferData,IntGetProcAddress("glClearBufferData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glClearBufferSubData,IntGetProcAddress("glClearBufferSubData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glCopyImageSubData,IntGetProcAddress("glCopyImageSubData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDebugMessageCallback,IntGetProcAddress("glDebugMessageCallback"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDebugMessageControl,IntGetProcAddress("glDebugMessageControl"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDebugMessageInsert,IntGetProcAddress("glDebugMessageInsert"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDispatchCompute,IntGetProcAddress("glDispatchCompute"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glDispatchComputeIndirect,IntGetProcAddress("glDispatchComputeIndirect"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glFramebufferParameteri,IntGetProcAddress("glFramebufferParameteri"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetDebugMessageLog,IntGetProcAddress("glGetDebugMessageLog"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetFramebufferParameteriv,IntGetProcAddress("glGetFramebufferParameteriv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetObjectLabel,IntGetProcAddress("glGetObjectLabel"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetObjectPtrLabel,IntGetProcAddress("glGetObjectPtrLabel"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramInterfaceiv,IntGetProcAddress("glGetProgramInterfaceiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramResourceIndex,IntGetProcAddress("glGetProgramResourceIndex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramResourceLocation,IntGetProcAddress("glGetProgramResourceLocation"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramResourceLocationIndex,IntGetProcAddress("glGetProgramResourceLocationIndex"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramResourceName,IntGetProcAddress("glGetProgramResourceName"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glGetProgramResourceiv,IntGetProcAddress("glGetProgramResourceiv"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glInvalidateBufferData,IntGetProcAddress("glInvalidateBufferData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glInvalidateBufferSubData,IntGetProcAddress("glInvalidateBufferSubData"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glInvalidateFramebuffer,IntGetProcAddress("glInvalidateFramebuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glInvalidateSubFramebuffer,IntGetProcAddress("glInvalidateSubFramebuffer"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glInvalidateTexImage,IntGetProcAddress("glInvalidateTexImage"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glInvalidateTexSubImage,IntGetProcAddress("glInvalidateTexSubImage"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMultiDrawArraysIndirect,IntGetProcAddress("glMultiDrawArraysIndirect"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glMultiDrawElementsIndirect,IntGetProcAddress("glMultiDrawElementsIndirect"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glObjectLabel,IntGetProcAddress("glObjectLabel"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glObjectPtrLabel,IntGetProcAddress("glObjectPtrLabel"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPopDebugGroup,IntGetProcAddress("glPopDebugGroup"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glPushDebugGroup,IntGetProcAddress("glPushDebugGroup"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glShaderStorageBlockBinding,IntGetProcAddress("glShaderStorageBlockBinding"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexBufferRange,IntGetProcAddress("glTexBufferRange"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexStorage2DMultisample,IntGetProcAddress("glTexStorage2DMultisample"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTexStorage3DMultisample,IntGetProcAddress("glTexStorage3DMultisample"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glTextureView,IntGetProcAddress("glTextureView"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribBinding,IntGetProcAddress("glVertexAttribBinding"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribFormat,IntGetProcAddress("glVertexAttribFormat"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribIFormat,IntGetProcAddress("glVertexAttribIFormat"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexAttribLFormat,IntGetProcAddress("glVertexAttribLFormat"))){
		++numFailed;
	}
	if(!set_loader_fptr(_ptrc_glVertexBindingDivisor,IntGetProcAddress("glVertexBindingDivisor"))){
		++numFailed;
	}
	return numFailed;
}

typedef int (*PFN_LOADFUNCPOINTERS)();
typedef struct ogl_StrToExtMap_s
{
	const char *extensionName;
	int *extensionVariable;
	PFN_LOADFUNCPOINTERS LoadExtension;
} ogl_StrToExtMap;

static ogl_StrToExtMap ExtensionMap[3] = {
    {"GL_EXT_texture_compression_s3tc", &ogl_ext_EXT_texture_compression_s3tc, nullptr},
    {"GL_EXT_texture_sRGB", &ogl_ext_EXT_texture_sRGB, nullptr},
    {"GL_EXT_texture_filter_anisotropic", &ogl_ext_EXT_texture_filter_anisotropic, nullptr},
};

static int g_extensionMapSize = 3;

static ogl_StrToExtMap *FindExtEntry(const char *extensionName)
{
	int loop;
	ogl_StrToExtMap *currLoc = ExtensionMap;
	for(loop = 0; loop < g_extensionMapSize; ++loop, ++currLoc)
	{
		if(strcmp(extensionName, currLoc->extensionName) == 0)
			return currLoc;
	}
	
    return nullptr;
}

static void ClearExtensionVars()
{
	ogl_ext_EXT_texture_compression_s3tc = ogl_LOAD_FAILED;
	ogl_ext_EXT_texture_sRGB = ogl_LOAD_FAILED;
	ogl_ext_EXT_texture_filter_anisotropic = ogl_LOAD_FAILED;
}


static void LoadExtByName(const char *extensionName)
{
    ogl_StrToExtMap *entry = nullptr;
	entry = FindExtEntry(extensionName);
	if(entry)
	{
		if(entry->LoadExtension)
		{
			int numFailed = entry->LoadExtension();
			if(numFailed == 0)
			{
				*(entry->extensionVariable) = ogl_LOAD_SUCCEEDED;
			}
			else
			{
				*(entry->extensionVariable) = ogl_LOAD_SUCCEEDED + numFailed;
			}
		}
		else
		{
			*(entry->extensionVariable) = ogl_LOAD_SUCCEEDED;
		}
	}
}


static void ProcExtsFromExtList()
{
	GLint iLoop;
	GLint iNumExtensions = 0;
	_ptrc_glGetIntegerv(GL_NUM_EXTENSIONS, &iNumExtensions);

	for(iLoop = 0; iLoop < iNumExtensions; iLoop++)
	{
		const char *strExtensionName = reinterpret_cast<const char*>(_ptrc_glGetStringi(GL_EXTENSIONS, GLuint(iLoop)));
		LoadExtByName(strExtensionName);
	}
}

int ogl_LoadFunctions()
{
	int numFailed = 0;
	ClearExtensionVars();
	
	if(!set_loader_fptr(_ptrc_glGetIntegerv,IntGetProcAddress("glGetIntegerv"))) return ogl_LOAD_FAILED;
	if(!set_loader_fptr(_ptrc_glGetStringi,IntGetProcAddress("glGetStringi"))) return ogl_LOAD_FAILED;
	
	ProcExtsFromExtList();
	numFailed = Load_Version_4_3();
	
	if(numFailed == 0)
		return ogl_LOAD_SUCCEEDED;
	else
		return ogl_LOAD_SUCCEEDED + numFailed;
}

static int g_major_version = 0;
static int g_minor_version = 0;

static void GetGLVersion()
{
	glGetIntegerv(GL_MAJOR_VERSION, &g_major_version);
	glGetIntegerv(GL_MINOR_VERSION, &g_minor_version);
}

int ogl_GetMajorVersion()
{
	if(g_major_version == 0)
		GetGLVersion();
	return g_major_version;
}

int ogl_GetMinorVersion()
{
	if(g_major_version == 0) //Yes, check the major version to get the minor one.
		GetGLVersion();
	return g_minor_version;
}

int ogl_IsVersionGEQ(int majorVersion, int minorVersion)
{
	if(g_major_version == 0)
		GetGLVersion();
		
	if(majorVersion > g_major_version) return 1;
	if(majorVersion < g_major_version) return 0;
	if(minorVersion >= g_minor_version) return 1;
	return 0;
}

